#include <stdint.h>

#include <xdc/runtime/Error.h>

#include <ti/sysbios/knl/Clock.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <icall.h>
#include <hal_assert.h>
#include <bcomdef.h>
/* Header files required to enable instruction fetch cache */
#include <inc/hw_memmap.h>
#include <driverlib/vims.h>
#ifndef USE_DEFAULT_USER_CFG
#include <ble_user_config.h>
// BLE user defined configuration
icall_userCfg_t user0Cfg = BLE_USER_CFG;
#endif // USE_DEFAULT_USER_CFG

#ifdef VVTK_NFC
#include "Application/nfc/nfc_task.h"
#endif
#ifdef VVTK_RFID
#include "Application/rfid/em4095.h"
#endif
#include "Application/system/log.h"
#include "Application/ble/ble.h"
#include "Application/tool/tool.h"
#include "lib/protobuf-c/accesscontrol.pb-c.h"
#include "Application/tool/tool.h"

static Task_Struct accessConrtolTask;

extern void AssertHandler(uint8_t assertCause,uint8_t assertSubcause);

static void access_control_task(UArg a0, UArg a1)
{
    Log_init();
    GPIO_init();
    SPI_init();

    VVTK_LOGI("\n\nSTART");
    uint8_t *package = ICall_malloc(32) ;
    Accesscontrol__Status status = ACCESSCONTROL__STATUS__INIT;
    status.nfcid = ICall_malloc(32);
    status.rfid = ICall_malloc(32);
    status.bleid = ICall_malloc(32);
    uint8_t *rfidResult = ICall_malloc(8);
    uint8_t rfidLen = 8;
    uint8_t *nfcResult = ICall_malloc(10);
    uint8_t nfcLen = 10;
    uint8_t *bleResult = ICall_malloc(6);
    uint8_t bleLen = 6;
    #ifdef VVTK_RFID
    SetupRFID();
    #endif
    #ifdef VVTK_NFC
    SetupNFC();
    #endif
    CreateBleTask();

    while(1)
    {
        #ifdef VVTK_RFID
        memset(rfidResult , 0 , rfidLen);
        LoopRFID();
        GetRFIDResult( rfidResult , &rfidLen);
        // SetRFIDCharValue(rfidResult , rfidLen);
        Sleep(100);
        #endif
        #ifdef VVTK_NFC
        LoopNFC();
        GetNFCResult(nfcResult , &nfcLen);
        // SetNFCCharValue(nfcResult , nfcLen);
        Sleep(100);
        #endif
        #ifdef VVTK_BLE_OBSERVER
        GetBLEResult(bleResult , &bleLen);
        // SetBLECharValue(bleResult , bleLen);
        #endif

        memset(status.nfcid , 0 , 32);
        memset(status.rfid , 0 , 32);
        memset(status.bleid , 0 , 32);
        Uint8ArrayToHexStr(nfcResult ,  nfcLen  , status.nfcid);
        Uint8ArrayToHexStr(rfidResult ,  rfidLen  , status.rfid);
        Uint8ArrayToHexStr(bleResult , bleLen  , status.bleid );

        int ret = -1 ;
        size_t package_size = accesscontrol__status__get_packed_size(&status);
        if(package_size <= 32)
        {
            memset(package , 0 , 32);
            ret=accesscontrol__status__pack(&status,package);
            if(ret <= 0)
            {
                VVTK_LOGE("accesscontrol__status__pack failed");
            }
            else
            {
                SetResultCharValue(package, package_size);
            }
        }
        else
        {
            VVTK_LOGE("package_size : %d too big" , package_size);
        }

        // Task_yield();
    }

    ICall_free(rfidResult);
    ICall_free(nfcResult);
    ICall_free(bleResult);
    ICall_free(package);
    ICall_free(status.nfcid);
    ICall_free(status.rfid);
    ICall_free(status.bleid);
    return (NULL);
}

int main()
{
    /* Register Application callback to trap asserts raised in the Stack */
    RegisterAssertCback(AssertHandler);

    Board_initGeneral();

    // Enable iCache prefetching
    VIMSConfigure(VIMS_BASE, TRUE, TRUE);
    // Enable cache
    VIMSModeSet(VIMS_BASE, VIMS_MODE_ENABLED);

#if !defined( POWER_SAVING )
    /* Set constraints for Standby, powerdown and idle mode */
    // PowerCC26XX_SB_DISALLOW may be redundant
    Power_setConstraint(PowerCC26XX_SB_DISALLOW);
    Power_setConstraint(PowerCC26XX_IDLE_PD_DISALLOW);
#endif // POWER_SAVING

    /* Update User Configuration of the stack */
    user0Cfg.appServiceInfo->timerTickPeriod = Clock_tickPeriod;
    user0Cfg.appServiceInfo->timerMaxMillisecond = ICall_getMaxMSecs();

    /* Initialize ICall module */
    ICall_init();

    /* Start tasks of external images - Priority 5 */
    ICall_createRemoteTasks();

    Task_Params taskParams;
    Task_Params_init(&taskParams);
    taskParams.stackSize = 2048;
    taskParams.priority = 1;
    Task_construct(&accessConrtolTask, access_control_task, &taskParams, NULL);

    /* enable interrupts and start SYS/BIOS */
    BIOS_start();

    return(0);
}

void AssertHandler(uint8_t assertCause, uint8_t assertSubcause)
{
    VVTK_LOGE(">>>STACK ASSERT Cause 0x%02x subCause 0x%02x",
               assertCause, assertSubcause);

    // check the assert cause
    switch(assertCause)
    {
    case HAL_ASSERT_CAUSE_OUT_OF_MEMORY:
        VVTK_LOGE("***ERROR***");
        VVTK_LOGE(">> OUT OF MEMORY!");
        break;

    case HAL_ASSERT_CAUSE_INTERNAL_ERROR:
        // check the subcause
        if(assertSubcause == HAL_ASSERT_SUBCAUSE_FW_INERNAL_ERROR)
        {
            VVTK_LOGE("***ERROR***");
            VVTK_LOGE(">> INTERNAL FW ERROR!");
        }
        else
        {
            VVTK_LOGE("***ERROR***");
            VVTK_LOGE(">> INTERNAL ERROR!");
        }
        break;

    case HAL_ASSERT_CAUSE_ICALL_ABORT:
        VVTK_LOGE("***ERROR***");
        VVTK_LOGE(">> ICALL ABORT!");
        //HAL_ASSERT_SPINLOCK;
        break;

    case HAL_ASSERT_CAUSE_ICALL_TIMEOUT:
        VVTK_LOGE("***ERROR***");
        VVTK_LOGE(">> ICALL TIMEOUT!");
        //HAL_ASSERT_SPINLOCK;
        break;

    case HAL_ASSERT_CAUSE_WRONG_API_CALL:
        VVTK_LOGE("***ERROR***");
        VVTK_LOGE(">> WRONG API CALL!");
        //HAL_ASSERT_SPINLOCK;
        break;

    default:
        VVTK_LOGE("***ERROR***");
        VVTK_LOGE(">> DEFAULT SPINLOCK!");
        //HAL_ASSERT_SPINLOCK;
    }

    return;
}
