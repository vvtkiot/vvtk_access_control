/*
*         Copyright (c), NXP Semiconductors Caen / France
*
*                     (C)NXP Semiconductors
*       All rights are reserved. Reproduction in whole or in part is
*      prohibited without the written consent of the copyright owner.
*  NXP reserves the right to make changes without notice at any time.
* NXP makes no warranty, expressed, implied or statutory, including but
* not limited to any implied warranty of merchantability or fitness for any
*particular purpose, or that the use will not infringe any third party patent,
* copyright or trademark. NXP must not be liable for any loss or damage
*                          arising from its use.
*/

#include <xdc/runtime/Timestamp.h>
#include <ti/drivers/PIN.h>
#include <ti_drivers_config.h>

#include "Application/tool/tool.h"
#include "Application/i2c/i2cTool.h"
#include "Application/system/log.h"

#define MAX_PAYLOAD_SIZE 255 // See NCI specification V1.0, section 3.1
#define MSG_HEADER_SIZE 3
#define SLAVE_ADDRESS 0x28

#define PN7150_RESET_PIN 0x04
#define PN7150_INTERRUP_PIN 0x05

static PIN_Handle interrupPinHandle;
static PIN_State interrupPinState;
static PIN_Handle resetPinHandle;
static PIN_State resetPinState;

static PIN_Config interrupPinTable[] = {
    PN7150_INTERRUP_PIN  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};

static PIN_Config resetPinTable[] = {
    PN7150_RESET_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

static void interrupCallbackFxn(PIN_Handle handle, PIN_Id pinId)
{
    VVTK_LOGI("i2cInterrupCallbackFxn................");
}

void tml_Connect(void)
{
    resetPinHandle = PIN_open(&resetPinState, resetPinTable);
    if(!resetPinHandle) {
        VVTK_LOGE( "open reset pin : %d error" , PN7150_RESET_PIN);
        while(1);
    }
    PIN_setOutputValue(resetPinHandle, PN7150_RESET_PIN, 1);

    interrupPinHandle = PIN_open(&interrupPinState, interrupPinTable);
    if(!interrupPinHandle) {
        VVTK_LOGE( "open interrup pin : %d error" , PN7150_INTERRUP_PIN);
        while(1);
    }

    // if (PIN_registerIntCb(interrupPinHandle, &interrupCallbackFxn) != 0) {
    //     VVTK_LOGE("PIN register Interrup callback failed");
    //     while(1);
    // }

    if(!I2CTool_init(CONFIG_I2C_NFC , I2C_1000kHz , SLAVE_ADDRESS))
    {
        VVTK_LOGE( "init i2c error");
    }
}

void tml_Disconnect(void)
{
	if(!I2CTool_close())
	{
		VVTK_LOGI("close i2c error");
	}
}

void tml_Send(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytesSent)
{
	if(!I2CTool_send(pBuffer , BufferLen))
    {
        VVTK_LOGW("send cmd failed");
        *pBytesSent = 0 ;
    }
    else
    {
        *pBytesSent = BufferLen ;
    }
}

void tml_Receive(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytes, uint16_t timeout)
{
	uint64_t ts_1, ts_2;
    xdc_runtime_Types_Timestamp64 ts64_1, ts64_2;
    xdc_runtime_Types_FreqHz freq;
    uint64_t delta;
    uint64_t deltaMS;
    Timestamp_getFreq(&freq);
    Timestamp_get64(&ts64_1);

    while(!I2CTool_read(pBuffer , BufferLen))
    {
        Timestamp_get64(&ts64_2);
        ts_1 = ((uint64_t)ts64_1.hi << 32) | ts64_1.lo;
        ts_2 = ((uint64_t)ts64_2.hi << 32) | ts64_2.lo;

        delta = ts_2 - ts_1;
        deltaMS = delta / (freq.lo / 1000);

        if(deltaMS > timeout)
        {
            *pBytes = 0;
            return;
        }
    }
    *pBytes = MSG_HEADER_SIZE + pBuffer[2]; //header+payload
    return;
}
