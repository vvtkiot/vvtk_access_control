#ifndef VVTK_I2C_I2CTool_H
#define VVTK_I2C_I2CTool_H

#include <stdbool.h>
#include <ti/drivers/I2C.h>

bool I2CTool_init(uint8_t index , I2C_BitRate bitRate , uint8_t slave_Address);
bool I2CTool_send(uint8_t* txBuffer , uint8_t writeCount);
bool I2CTool_read(uint8_t* rxBuffer , size_t readCount);
bool I2CTool_close();

#endif