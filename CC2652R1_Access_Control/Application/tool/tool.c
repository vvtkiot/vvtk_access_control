#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>

#include "Application/tool/tool.h"

void Sleep(unsigned int ms)
{
    Task_sleep(ms*1000 / Clock_tickPeriod);
}

void Uint8ArrayToHexStr(uint8_t *array , uint8_t size , char* str)
{
    strcat(str ,"0x" );
    int i ;
    for(i = 0 ; i < size ; i ++)
    {
        char tmp[2] = {0};
        _itoa(array[i] , tmp , 16);
        strcat(str ,tmp );
    }
}