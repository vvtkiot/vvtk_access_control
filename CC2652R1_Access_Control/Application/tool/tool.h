#ifndef VVTK_TOOL_H
#define VVTK_TOOL_H

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>


void Sleep (unsigned int ms);
void Uint8ArrayToHexStr(uint8_t *array , uint8_t size , char* str);

#endif
