#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Queue.h>
#include <icall_ble_api.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/BIOS.h>
#include <util.h>

#ifdef USE_RCOSC
#include <rcosc_calibration.h>
#endif //USE_RCOSC
#include "Application/system/log.h"
#include "Application/ble/peripheral.h"
#ifdef VVTK_BLE_OBSERVER
#include "Application/ble/observer.h"
#endif
#include "Application/ble/bleCommonDef.h"
#include "Application/ble/PROFILES/gatt_profile.h"

// Task configuration
#define TASK_PRIORITY                     2

#ifndef TASK_STACK_SIZE
#define TASK_STACK_SIZE                   2048
#endif

// Internal Events for RTOS application
#define SP_ICALL_EVT                         ICALL_MSG_EVENT_ID // Event_Id_31
#define SP_QUEUE_EVT                         UTIL_QUEUE_EVENT_ID // Event_Id_30

// Bitwise OR of all RTOS events to pend on
#define SP_ALL_EVENTS                        (SP_ICALL_EVT             | \
                                              SP_QUEUE_EVT)

// Task configuration
Task_Struct bleTask;
#if defined __TI_COMPILER_VERSION__
#pragma DATA_ALIGN(bleTaskStack, 8)
#else
#pragma data_alignment=8
#endif

// App event passed from stack modules. This type is defined by the application
// since it can queue events to itself however it wants.
typedef struct
{
  uint8_t event;                // event type
  void    *pData;               // pointer to message
} spEvt_t;

uint8_t bleTaskStack[TASK_STACK_SIZE];
// Queue object used for app messages
static Queue_Struct appMsgQueue;
static Queue_Handle appMsgQueueHandle;

// Entity ID globally used to check for source and/or destination of messages
static ICall_EntityID selfEntity;

// Event globally used to post local events and pend on system and
// local events.
static ICall_SyncHandle syncEvent;
static GapScan_Evt_AdvRpt_t closestBleDevice;
static uint8_t bleLen;
static Semaphore_Struct advRpt_semStruct;
static Semaphore_Handle advRpt_semHandle;

static uint8_t processStackMsg(ICall_Hdr *pMsg)
{
    Peripheral_processStackMsg(pMsg);
    #ifdef VVTK_BLE_OBSERVER
    Observer_processStackMsg(pMsg);
    #endif
    return TRUE;
}

static void processAdvEvent(spGapAdvEventData_t *pEventData)
{
    switch (pEventData->event)
    {
        case GAP_EVT_ADV_START_AFTER_ENABLE:
            VVTK_LOGI( "Adv Set %d Enabled",*(uint8_t *)(pEventData->pBuf));
            break;

        case GAP_EVT_ADV_END_AFTER_DISABLE:
            VVTK_LOGI( "Adv Set %d Disabled",*(uint8_t *)(pEventData->pBuf));
            break;

        case GAP_EVT_ADV_START:
            break;

        case GAP_EVT_ADV_END:
            break;

        case GAP_EVT_ADV_SET_TERMINATED:
        {
        #ifndef Display_DISABLE_ALL
            GapAdv_setTerm_t *advSetTerm = (GapAdv_setTerm_t *)(pEventData->pBuf);
        #endif
            VVTK_LOGI("Adv Set %d disabled after conn %d",advSetTerm->handle, advSetTerm->connHandle );
        }
        break;

        case GAP_EVT_SCAN_REQ_RECEIVED:
            break;

        case GAP_EVT_INSUFFICIENT_MEMORY:
            break;

        default:
            break;
    }

    // All events have associated memory to free except the insufficient memory
    if (pEventData->event != GAP_EVT_INSUFFICIENT_MEMORY)
    {
        ICall_free(pEventData->pBuf);
    }
}

static void processPairState(spPairStateData_t *pPairData)
{
    uint8_t state = pPairData->state;
    uint8_t status = pPairData->status;

    switch (state)
    {
        case GAPBOND_PAIRING_STATE_STARTED:
            VVTK_LOGI( "Pairing started");
            break;

        case GAPBOND_PAIRING_STATE_COMPLETE:
            if (status == SUCCESS)
            {
                VVTK_LOGI("Pairing success");
            }
            else
            {
                VVTK_LOGI("Pairing fail: %d", status);
            }
            break;

        case GAPBOND_PAIRING_STATE_ENCRYPTED:
            if (status == SUCCESS)
            {
                VVTK_LOGI("Encryption success");
            }
            else
            {
                VVTK_LOGI("Encryption failed: %d", status);
            }
            break;

        case GAPBOND_PAIRING_STATE_BOND_SAVED:
            if (status == SUCCESS)
            {
                VVTK_LOGI("Bond save success");
            }
            else
            {
                VVTK_LOGI("Bond save failed: %d", status);
            }
            break;

        default:
            break;
    }
}

static void processPasscode(spPasscodeData_t *pPasscodeData)
{
    // Display passcode to user
    if (pPasscodeData->uiOutputs != 0)
    {
        VVTK_LOGI( "Passcode: %d",B_APP_DEFAULT_PASSCODE);
    }

    #if defined(GAP_BOND_MGR)
        // Send passcode response
        GAPBondMgr_PasscodeRsp(pPasscodeData->connHandle , SUCCESS,
                                B_APP_DEFAULT_PASSCODE);
    #endif
}

static void processAppMsg(spEvt_t *pMsg)
{
    bool dealloc = TRUE;

    switch (pMsg->event)
    {
        case SP_ADV_EVT:
            processAdvEvent((spGapAdvEventData_t*)(pMsg->pData));
            break;

        case SP_PAIR_STATE_EVT:
            processPairState((spPairStateData_t*)(pMsg->pData));
            break;

        case SP_PASSCODE_EVT:
            processPasscode((spPasscodeData_t*)(pMsg->pData));
            break;

        case SP_PERIODIC_EVT:
            #ifdef VVTK_BLE_OBSERVER
            ScanEnable();
            #endif
            break;

        case SP_READ_RPA_EVT:
            Peripheral_updateRPA();
            break;

        case SP_SEND_PARAM_UPDATE_EVT:
        {
            // Extract connection handle from data
            uint16_t connHandle = *(uint16_t *)(((spClockEventData_t *)pMsg->pData)->data);

            Peripheral_processParamUpdate(connHandle);

            // This data is not dynamically allocated
            dealloc = FALSE;
            break;
        }

        case SP_NOTIFY_RESULT:
        {
            SimpleProfile_NotifyCharResult();
            break;
        }

        case SC_EVT_SCAN_ENABLED:
            // VVTK_LOGI("SC_EVT_SCAN_ENABLED");
            Semaphore_pend(advRpt_semHandle, BIOS_WAIT_FOREVER);
            bleLen = 0 ;
            memset(&closestBleDevice , 0x00 ,  sizeof(GapScan_Evt_AdvRpt_t));
            closestBleDevice.rssi = SCAN_RSSI_MIN;
            Semaphore_post(advRpt_semHandle);
            break;

        case SC_EVT_ADV_REPORT :
            GapScan_Evt_AdvRpt_t* pAdvRpt = (GapScan_Evt_AdvRpt_t*) (pMsg->pData);
            if (pAdvRpt->pData != NULL)
            {
                if(strstr(pAdvRpt->pData , "BLE Device")!=NULL)
                {
                    VVTK_LOGI("device : %s , rssi : %d , type : %x",Util_convertBdAddr2Str(pAdvRpt->addr) , pAdvRpt->rssi ,pAdvRpt->addrType );
                    VVTK_LOGI("======================================================\n")
                    Semaphore_pend(advRpt_semHandle, BIOS_WAIT_FOREVER);
                    if(pAdvRpt->rssi > closestBleDevice.rssi)
                    {
                        memcpy(&closestBleDevice , pAdvRpt , sizeof(GapScan_Evt_AdvRpt_t));
                        bleLen = B_ADDR_LEN ;
                    }
                    Semaphore_post(advRpt_semHandle);
                }
                ICall_free(pAdvRpt->pData);
            }
            break;
        case SC_EVT_SCAN_DISABLED :
            // VVTK_LOGI("SC_EVT_SCAN_DISABLED");

            // uint8_t numReport = ((GapScan_Evt_End_t*) (pMsg->pData))->numReport;
            // VVTK_LOGI("numReport : %d" , numReport);
            // GapScan_Evt_AdvRpt_t advRpt;
            // int i = 0 ;
            // for(i=0 ; i < numReport ;i++)
            // {
            //     GapScan_getAdvReport(i, &advRpt);
            //     // if(advRpt.addr[0] == 0x75 || advRpt.addr[0] == 0x75)
            //     // {
            //     //     VVTK_LOGI("device : %s  , type : %x"  ,  Util_convertBdAddr2Str(advRpt.addr) ,advRpt.addrType);
            //     // }

            //     VVTK_LOGI("addrType : %x , addr : %s  , rssi : %d"  ,  advRpt.addrType , Util_convertBdAddr2Str(advRpt.addr) ,advRpt.rssi);
            //     // VVTK_LOGI("device : %x  , type : %x"  ,  advRpt.addr[0] ,advRpt.addrType);

            // }
            #ifdef VVTK_BLE_OBSERVER
            Rescan();
            break ;
            #endif
        case SC_EVT_INSUFFICIENT_MEM :
            VVTK_LOGI("SC_EVT_INSUFFICIENT_MEM");
            #ifdef VVTK_BLE_OBSERVER
            ScanDisable();
            #endif
            break;

        default:
            // Do nothing.
            break;
    }

    // Free message data if it exists and we are to dealloc
    if ((dealloc == TRUE) && (pMsg->pData != NULL))
    {
        ICall_free(pMsg->pData);
    }
}

void SetupBLE()
{
    Semaphore_Params semParams;
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    Semaphore_construct(&advRpt_semStruct, 1, &semParams);
    advRpt_semHandle = Semaphore_handle(&advRpt_semStruct);
    bleLen = 0 ;
    closestBleDevice.rssi =  SCAN_RSSI_MIN ;
    ICall_registerApp(&selfEntity, &syncEvent);
    appMsgQueueHandle = Util_constructQueue(&appMsgQueue);
    InitPeripheral(&selfEntity);
}

void LoopBLE()
{
    uint32_t events;

    // Waits for an event to be posted associated with the calling thread.
    // Note that an event associated with a thread is posted when a
    // message is queued to the message receive queue of the thread
    // events = Event_pend(syncEvent, Event_Id_NONE, SP_ALL_EVENTS,ICALL_TIMEOUT_FOREVER);
    events = Event_pend(syncEvent, Event_Id_NONE, SP_ALL_EVENTS,ICALL_TIMEOUT_PREDEFINE);
    if (events)
    {
        ICall_EntityID dest;
        ICall_ServiceEnum src;
        ICall_HciExtEvt *pMsg = NULL;

        // Fetch any available messages that might have been sent from the stack
        if (ICall_fetchServiceMsg(&src, &dest,
                                (void **)&pMsg) == ICALL_ERRNO_SUCCESS)
        {
            uint8 safeToDealloc = TRUE;

            if ((src == ICALL_SERVICE_CLASS_BLE) && (dest == selfEntity))
            {
                ICall_Stack_Event *pEvt = (ICall_Stack_Event *)pMsg;

                // Check for BLE stack events first
                if (pEvt->signature != 0xffff)
                {
                    // Process inter-task message
                    safeToDealloc = processStackMsg((ICall_Hdr *)pMsg);
                }
            }

            if (pMsg && safeToDealloc)
            {
                ICall_freeMsg(pMsg);
            }
        }

        // If RTOS queue is not empty, process app message.
        if (events & SP_QUEUE_EVT)
        {
            while (!Queue_empty(appMsgQueueHandle))
            {
                spEvt_t *pMsg = (spEvt_t *)Util_dequeueMsg(appMsgQueueHandle);
                if (pMsg)
                {
                    // Process message.
                    processAppMsg(pMsg);

                    // Free the space from the message.
                    ICall_free(pMsg);
                }
            }
        }
    }
}

static void bleTaskFxn(UArg a0, UArg a1)
{
    // Initialize application
    // Log_init();
    SetupBLE();
    while(1)
    {
        LoopBLE();
        Task_yield();
    }
}

status_t BLE_EnqueueMsg(uint8_t event, void *pData)
{
  uint8_t success;
  spEvt_t *pMsg = ICall_malloc(sizeof(spEvt_t));

  // Create dynamic pointer to message.
  if(pMsg)
  {
    pMsg->event = event;
    pMsg->pData = pData;

    // Enqueue the message.
    success = Util_enqueueMsg(appMsgQueueHandle, syncEvent, (uint8_t *)pMsg);
    return (success) ? SUCCESS : FAILURE;
  }

  return(bleMemAllocError);
}

void CreateBleTask(void)
{
    Task_Params taskParams;

    // Configure task
    Task_Params_init(&taskParams);
    taskParams.stack = bleTaskStack;
    taskParams.stackSize = TASK_STACK_SIZE;
    taskParams.priority = TASK_PRIORITY;

    Task_construct(&bleTask, bleTaskFxn, &taskParams, NULL);
}


static bool compareArrays( uint8 *a ,  uint8 *b , int n)
{
  int i = 0;
  for(i=0 ; i < n ; i++)
  {
    if(a[i]!=b[i])
    {
      return false;
    }
  }
  return true;
}

#ifdef VVTK_NFC
void SetNFCCharValue(void *value , uint8 len)
{
    Peripheral_setNFCCharValue(value ,len);
}
#endif
#ifdef VVTK_RFID
void SetRFIDCharValue(void *value , uint8 len)
{
    Peripheral_setRFIDCharValue(value , len);
}
#endif
#ifdef VVTK_BLE_OBSERVER
void SetBLECharValue(void *value , uint8 len)
{
    Peripheral_setBLECharValue(value ,len);
}
#endif

void SetResultCharValue(void *value , uint8 len)
{
    uint8_t oldValue[32];
    SimpleProfile_GetParameter( RESULT_CHAR , oldValue);
    if(compareArrays(value , oldValue , len) == false)
    {
        Peripheral_setResultCharValue(value ,len);
        if(BLE_EnqueueMsg(SP_NOTIFY_RESULT, NULL) != SUCCESS )
        {
            VVTK_LOGE("BLE_EnqueueMsg failed");
        }
    }
}

void GetBLEResult(uint8_t* data , uint8_t* len)
{
    Semaphore_pend(advRpt_semHandle, BIOS_WAIT_FOREVER);
    memcpy(data ,  closestBleDevice.addr , bleLen);
    *len = bleLen;
    Semaphore_post(advRpt_semHandle);
}