/******************************************************************************

 @file  simple_gatt_profile.c

 @brief This file contains the Simple GATT profile sample GATT service profile
        for use with the BLE sample application.

 Group: WCS, BTS
 Target Device: cc2640r2

 ******************************************************************************

 Copyright (c) 2010-2021, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************


 *****************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include <icall.h>
#include "util.h"
/* This Header file contains all BLE API and icall structure definition */
#include "icall_ble_api.h"

#ifdef SYSCFG
#include "ti_ble_config.h"
#ifdef USE_GATT_BUILDER
#include "ti_ble_gatt_service.h"
#endif
#endif

#include "Application/ble/PROFILES/gatt_profile.h"
#include "Application/system/log.h"


/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        14

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
// Simple GATT Profile Service UUID: 0xFFF0
static CONST uint8 serviceUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(ACCESS_CONTROL_SERV_UUID), HI_UINT16(ACCESS_CONTROL_SERV_UUID)
};

// Characteristic 1 UUID: 0xFFF1 (NFC)
static CONST uint8 nfcCharUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(NFC_CHAR_UUID), HI_UINT16(NFC_CHAR_UUID)
};

// Characteristic 2 UUID: 0xFFF2 (RFID)
static CONST uint8 rfidCharUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(RFID_CHAR_UUID), HI_UINT16(RFID_CHAR_UUID)
};

// Characteristic 3 UUID: 0xFFF3 (BLE)
static CONST uint8 bleCharUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(BLE_CHAR_UUID), HI_UINT16(BLE_CHAR_UUID)
};

// Characteristic 4 UUID: 0xFFF4 (Result)
static CONST uint8 resultCharUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(RESULT_CHAR_UUID), HI_UINT16(RESULT_CHAR_UUID)
};
/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

/*********************************************************************
 * Profile Attributes - variables
 */

// Access Control Profile Service attribute
static CONST gattAttrType_t accessControlService = { ATT_BT_UUID_SIZE, serviceUUID };

static uint8 nfcCharProps =  GATT_PROP_READ;
static uint8 nfcCharValue[CHAR_VALUE_LEN] = {0};
static uint8 nfcCharValueLen = 0;
static uint8 nfcCharUserDesp[32] = "NFC Characteristic";

static uint8 rfidCharProps =  GATT_PROP_READ;
static uint8 rfidCharValue[CHAR_VALUE_LEN] = {0};
static uint8 rfidCharValueLen = 0;
static uint8 rfidCharUserDesp[32] = "RFID Characteristic";

static uint8 bleCharProps =  GATT_PROP_READ;
static uint8 bleCharValue[CHAR_VALUE_LEN] = {0};
static uint8 bleCharValueLen = 0 ;
static uint8 bleCharUserDesp[32] = "BLE Characteristic";

static uint8 resultCharProps = GATT_PROP_NOTIFY | GATT_PROP_READ;
static uint8 resultCharValue[CHAR_VALUE_LEN] = {0};
static uint8 resultCharValueLen = 0 ;
static gattCharCfg_t *resultCharConfig;
static uint8 resultCharUserDesp[32] = "Result Characteristic";

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t simpleProfileAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] =
{
    // Simple Profile Service
    {
      { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
      GATT_PERMIT_READ,                         /* permissions */
      0,                                        /* handle */
      (uint8 *)&accessControlService            /* pValue */
    },
    //NFC Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &nfcCharProps
    },
    //NFC Characteristic Value
    {
      { ATT_BT_UUID_SIZE, nfcCharUUID },
      GATT_PERMIT_READ,
      0,
      nfcCharValue
    },
    //NFC Characteristic User Description
    {
      { ATT_BT_UUID_SIZE, charUserDescUUID },
      GATT_PERMIT_READ,
      0,
      nfcCharUserDesp
    },

    //RFID Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &rfidCharProps
    },
    //RFID Characteristic Value
    {
      { ATT_BT_UUID_SIZE, rfidCharUUID },
      GATT_PERMIT_READ,
      0,
      rfidCharValue
    },
    //RFID Characteristic User Description
    {
      { ATT_BT_UUID_SIZE, charUserDescUUID },
      GATT_PERMIT_READ,
      0,
      rfidCharUserDesp
    },

    //BLE Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &bleCharProps
    },
    //BLE Characteristic Value
    {
      { ATT_BT_UUID_SIZE, bleCharUUID },
      GATT_PERMIT_READ,
      0,
      bleCharValue
    },
    //BLE Characteristic User Description
    {
      { ATT_BT_UUID_SIZE, charUserDescUUID },
      GATT_PERMIT_READ,
      0,
      bleCharUserDesp
    },
    //Result Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &resultCharProps
    },
    //Result Characteristic Value
    {
      { ATT_BT_UUID_SIZE, resultCharUUID },
      0 | GATT_PERMIT_READ,
      0,
      resultCharValue
    },
    //Result Characteristic configuration
    {
      { ATT_BT_UUID_SIZE, clientCharCfgUUID },
      GATT_PERMIT_READ | GATT_PERMIT_WRITE,
      0,
      (uint8 *)&resultCharConfig
    },
    //Result Characteristic User Description
    {
      { ATT_BT_UUID_SIZE, charUserDescUUID },
      GATT_PERMIT_READ,
      0,
      resultCharUserDesp
    },
};

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t service_ReadAttrCB(uint16_t connHandle,
                                          gattAttribute_t *pAttr,
                                          uint8_t *pValue, uint16_t *pLen,
                                          uint16_t offset, uint16_t maxLen,
                                          uint8_t method);
static bStatus_t service_WriteAttrCB(uint16_t connHandle,
                                           gattAttribute_t *pAttr,
                                           uint8_t *pValue, uint16_t len,
                                           uint16_t offset, uint8_t method);

/*********************************************************************
 * PROFILE CALLBACKS
 */

// Simple Profile Service Callbacks
// Note: When an operation on a characteristic requires authorization and
// pfnAuthorizeAttrCB is not defined for that characteristic's service, the
// Stack will report a status of ATT_ERR_UNLIKELY to the client.  When an
// operation on a characteristic requires authorization the Stack will call
// pfnAuthorizeAttrCB to check a client's authorization prior to calling
// pfnReadAttrCB or pfnWriteAttrCB, so no checks for authorization need to be
// made within these functions.
static CONST gattServiceCBs_t gattServiceCBs =
{
  service_ReadAttrCB,  // Read callback function pointer
  service_WriteAttrCB, // Write callback function pointer
  NULL                       // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      SimpleProfile_AddService
 *
 * @brief   Initializes the Simple Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t SimpleProfile_AddService( uint32 services )
{
  uint8 status;

  // Allocate Client Characteristic Configuration table
  resultCharConfig = (gattCharCfg_t *)ICall_malloc( sizeof(gattCharCfg_t) *MAX_NUM_BLE_CONNS );
  if ( resultCharConfig == NULL )
  {
    return ( bleMemAllocError );
  }

  // Initialize Client Characteristic Configuration attributes
  GATTServApp_InitCharCfg( LINKDB_CONNHANDLE_ALL, resultCharConfig );

  if ( services & ACCESS_CONTROL_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( simpleProfileAttrTbl,
                                          GATT_NUM_ATTRS( simpleProfileAttrTbl ),
                                          GATT_MAX_ENCRYPT_KEY_SIZE,
                                          &gattServiceCBs );
  }
  else
  {
    status = SUCCESS;
  }

  return ( status );
}

/*********************************************************************
 * @fn      SimpleProfile_SetParameter
 *
 * @brief   Set a Simple Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to write
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t SimpleProfile_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case NFC_CHAR:
      if(len <= CHAR_VALUE_LEN)
      {
          memset(nfcCharValue , 0 , CHAR_VALUE_LEN);
          nfcCharValueLen = len ;
          memcpy(nfcCharValue , value , len);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
      case RFID_CHAR:
        if(len <= CHAR_VALUE_LEN)
        {
            memset(rfidCharValue , 0 , CHAR_VALUE_LEN);
            rfidCharValueLen = len ;
            memcpy(rfidCharValue , value , len);
        }
        else
        {
          ret = bleInvalidRange;
        }
        break;
      case BLE_CHAR:
        if(len <= CHAR_VALUE_LEN)
        {
            memset(bleCharValue , 0 , CHAR_VALUE_LEN);
            bleCharValueLen = len ;
            memcpy(bleCharValue , value , len);
        }
        else
        {
          ret = bleInvalidRange;
        }
        break;
        case RESULT_CHAR:
          if(len <= CHAR_VALUE_LEN)
          {
              memset(resultCharValue , 0 , CHAR_VALUE_LEN);
              resultCharValueLen = len ;
              memcpy(resultCharValue , value , len);
          }
          else
          {
            ret = bleInvalidRange;
          }
        break;
    default:
      ret = INVALIDPARAMETER;
      break;
  }
  return ( ret );
}


bStatus_t SimpleProfile_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case NFC_CHAR:
      memcpy(value , nfcCharValue , nfcCharValueLen);
      break;

    case RFID_CHAR:
      memcpy(value , rfidCharValue , rfidCharValueLen);
      break;

    case BLE_CHAR:
      memcpy(value , bleCharValue , bleCharValueLen);
      break;

    case RESULT_CHAR:
      memcpy(value , resultCharValue , resultCharValueLen);
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

void SimpleProfile_NotifyCharResult()
{
    GATTServApp_ProcessCharCfg( resultCharConfig, resultCharValue, FALSE,
                                simpleProfileAttrTbl, GATT_NUM_ATTRS( simpleProfileAttrTbl ),
                                INVALID_TASK_ID, service_ReadAttrCB );
}

static bStatus_t service_ReadAttrCB(uint16_t connHandle,
                                          gattAttribute_t *pAttr,
                                          uint8_t *pValue, uint16_t *pLen,
                                          uint16_t offset, uint16_t maxLen,
                                          uint8_t method)
{

  bStatus_t status = SUCCESS;

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those reads
      case NFC_CHAR_UUID:
        // VVTK_LOGI( "----------------------NFC_CHAR_UUID");
        *pLen = nfcCharValueLen;
        memcpy(pValue , pAttr->pValue , nfcCharValueLen);
        break;
      case RFID_CHAR_UUID:
        // VVTK_LOGI("----------------------RFID_CHAR_UUID");
        *pLen = rfidCharValueLen;
        memcpy(pValue , pAttr->pValue , rfidCharValueLen);
        break;
      case BLE_CHAR_UUID:
        // VVTK_LOGI( "----------------------BLE_CHAR_UUID");
        *pLen = bleCharValueLen;
        memcpy(pValue , pAttr->pValue , bleCharValueLen);
        break;
      case RESULT_CHAR_UUID:
        // VVTK_LOGI( "----------------------RESULT_CHAR_UUID");
        *pLen = resultCharValueLen;
        memcpy(pValue , pAttr->pValue , resultCharValueLen);
        break;
      default:
        // Should never get here! (characteristics 3 and 4 do not have read permissions)
        *pLen = 0;
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    *pLen = 0;
    status = ATT_ERR_INVALID_HANDLE;
  }

  return ( status );
}

/*********************************************************************
 * @fn      service_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t service_WriteAttrCB(uint16_t connHandle,
                                           gattAttribute_t *pAttr,
                                           uint8_t *pValue, uint16_t len,
                                           uint16_t offset, uint8_t method)
{
  bStatus_t status = SUCCESS;

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      case GATT_CLIENT_CHAR_CFG_UUID:
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        break;

      default:
        // Should never get here! (characteristics 2 and 4 do not have write permissions)
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    status = ATT_ERR_INVALID_HANDLE;
  }

  return ( status );
}

/*********************************************************************
*********************************************************************/
