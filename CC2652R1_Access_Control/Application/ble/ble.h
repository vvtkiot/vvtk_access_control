#ifndef VVTK_BLE_H
#define VVTK_BLE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <icall_ble_api.h>

void CreateBleTask(void);
extern status_t BLE_EnqueueMsg(uint8_t event, void *pData);

#ifdef VVTK_NFC
void SetNFCCharValue(void *value , uint8 len);
#endif
#ifdef VVTK_RFID
void SetRFIDCharValue(void *value , uint8 len);
#endif
#ifdef VVTK_BLE_OBSERVER
void SetBLECharValue(void *value , uint8 len);
void GetBLEResult(uint8_t* data , uint8_t* len);
#endif
void SetResultCharValue(void *value , uint8 len);

#ifdef __cplusplus
}
#endif

#endif
