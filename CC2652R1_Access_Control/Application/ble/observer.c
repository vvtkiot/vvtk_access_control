#include <ti/sysbios/knl/Clock.h>
#include <icall_ble_api.h>
#include <util.h>
#include <ti_ble_config.h>

#include "Application/system/log.h"
#include "Application/ble/bleCommonDef.h"

// Advertising report fields to keep in the list
// Interested in only peer address type and peer address
#define SC_ADV_RPT_FIELDS   (SCAN_ADVRPT_FLD_EVENTTYPE | SCAN_ADVRPT_FLD_ADDRTYPE | SCAN_ADVRPT_FLD_ADDRESS | SCAN_ADVRPT_FLD_RSSI )

// How often to perform periodic event (in ms)
#define SCAN_PERIODIC_EVT_PERIOD 500

// Maximum number of scan results.
#define DEFAULT_MAX_SCAN_RES 8


static Clock_Struct clkPeriodic;
// Memory to pass periodic event ID to clock handler
static spClockEventData_t argPeriodic = { .event = SP_PERIODIC_EVT };

extern status_t BLE_EnqueueMsg(uint8_t event, void *pData);

/*********************************************************************
 * @fn      scanCb
 *
 * @brief   Callback called by GapScan module
 *
 * @param   evt - event
 * @param   msg - message coming with the event
 * @param   arg - user argument
 *
 * @return  none
 */
static void scanCb(uint32_t evt, void* pMsg, uintptr_t arg)
{
    uint8_t event;

    if (evt & GAP_EVT_ADV_REPORT)
    {
        event = SC_EVT_ADV_REPORT;
    }
    else if (evt & GAP_EVT_SCAN_ENABLED)
    {
        event = SC_EVT_SCAN_ENABLED;
    }
    else if (evt & GAP_EVT_SCAN_DISABLED)
    {
        event = SC_EVT_SCAN_DISABLED;
    }
    else if (evt & GAP_EVT_INSUFFICIENT_MEMORY)
    {
        event = SC_EVT_INSUFFICIENT_MEM;
    }
    else
    {
        return;
    }

    if(BLE_EnqueueMsg(event, pMsg) != SUCCESS)
    {
        ICall_free(pMsg);
    }
}

static void clockHandler(UArg arg)
{
    spClockEventData_t *pData = (spClockEventData_t *)arg;

    if (pData->event == SP_PERIODIC_EVT)
    {
        BLE_EnqueueMsg(SP_PERIODIC_EVT, NULL);
    }
}

void Obsercer_processGapMsg(gapEventHdr_t *pMsg)
{
    switch (pMsg->opcode)
    {
        case GAP_DEVICE_INIT_DONE_EVENT:
        {
            uint8_t temp8;
            uint16_t temp16;
            // Setup scanning
            // For more information, see the GAP section in the User's Guide:
            // http://software-dl.ti.com/lprf/ble5stack-latest/

            // Register callback to process Scanner events
            GapScan_registerCb(scanCb, NULL);

            // Set Scanner Event Mask
            GapScan_setEventMask(GAP_EVT_SCAN_ENABLED | GAP_EVT_SCAN_DISABLED | GAP_EVT_ADV_REPORT | GAP_EVT_INSUFFICIENT_MEMORY);

            // Set Scan PHY parameters
            GapScan_setPhyParams(DEFAULT_SCAN_PHY, SCAN_TYPE_PASSIVE,SCAN_PARAM_DFLT_INTERVAL, SCAN_PARAM_DFLT_INTERVAL);

            // Set Advertising report fields to keep
            temp16 = ADV_RPT_FIELDS;
            GapScan_setParam(SCAN_PARAM_RPT_FIELDS, &temp16);
            // Set Scanning Primary PHY
            temp8 = DEFAULT_SCAN_PHY;
            GapScan_setParam(SCAN_PARAM_PRIM_PHYS, &temp8);
            // Set LL Duplicate Filter
            temp8 = SCAN_FLT_DUP_ENABLE;
            GapScan_setParam(SCAN_PARAM_FLT_DUP, &temp8);

            int8_t minRssi = -50;
            GapScan_setParam(SCAN_PARAM_FLT_MIN_RSSI , &minRssi);

            // Set PDU type filter -
            // Only 'Connectable' and 'Complete' packets are desired.
            // It doesn't matter if received packets are
            // whether Scannable or Non-Scannable, whether Directed or Undirected,
            // whether Scan_Rsp's or Advertisements, and whether Legacy or Extended.
            temp16 = SCAN_FLT_PDU_CONNECTABLE_ONLY | SCAN_FLT_PDU_COMPLETE_ONLY;
            GapScan_setParam(SCAN_PARAM_FLT_PDU_TYPE, &temp16);

            // Create one-shot clock for internal periodic events.
            Util_constructClock(&clkPeriodic, clockHandler,SCAN_PERIODIC_EVT_PERIOD, 0 , false, (UArg)&argPeriodic);

            Util_startClock(&clkPeriodic);
            break;
        }
        default:
            break;
    }
}

void Observer_processStackMsg(void *pMsg)
{
    switch (((ICall_Hdr *)pMsg)->event)
    {
        case GAP_MSG_EVENT:
            Obsercer_processGapMsg((gapEventHdr_t*) pMsg);
            break;
        default:
            // do nothing
            break;
    }
}

void Rescan()
{
    Util_startClock(&clkPeriodic);
}

void ScanEnable()
{
    GapScan_enable(0, DEFAULT_SCAN_DURATION, DEFAULT_MAX_SCAN_RES);
}

void ScanDisable()
{
    GapScan_disable();
}

