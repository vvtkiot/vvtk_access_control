#ifndef VVTK_OBSERVER_H
#define VVTK_OBSERVER_H

#ifdef __cplusplus
extern "C"
{
#endif

void ScanEnable();
void ScanDisable();
void Observer_processStackMsg(void *pMsg);
void Rescan();

#ifdef __cplusplus
}
#endif

#endif
