#include <ti/sysbios/BIOS.h>
#include <ti/drivers/SPI.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti_drivers_config.h>
#include <icall.h>

#include "Application/system/log.h"
#include "Application/tool/tool.h"
#include "Application/rfid/manchester.h"
#include "Application/rfid/em4095.h"

#define SPI_MSG_LENGTH  (1024)
#define RESULT_SIZE 64

static Semaphore_Struct semStruct;
static Semaphore_Handle semHandle;
static SPI_Handle slaveSpi;
static uint8_t slaveRxBuffer[SPI_MSG_LENGTH];
static uint32_t bufSize ;
static uint8_t *buf ;
static uint8_t *result ;
static uint8_t resultLen ;

void transferCompleteFxn(SPI_Handle handle, SPI_Transaction *transaction)
{
    Semaphore_post(semHandle);
}

static bool sampleData( RFType type, uint8_t* buf , uint32_t bufSize , uint8_t *result ,  uint32_t resultSize)
{
    int i ;
    int index =0 ;
    uint8_t times ;
    if(type == RF32)
    {
        if(bufSize * 8 /32  > resultSize)
        {
            return false;
        }
        times = 4 ;
    }
    else if(type == RF64)
    {
        if(bufSize * 8 /64  > resultSize)
        {
            return false;
        }
        times = 8 ;
    }

    for(i = 0 ; i < bufSize ; )
    {
        uint8_t highCount = 0 ;
        uint8_t lowCount = 0 ;
        int j;
        for(j=0;j<times;j++)
        {
            if(i >=  bufSize)
            {
                return false;
            }
            int shift ;
            for(shift = 7 ; shift >= 0 ; shift --)
            {
                uint8_t value = ( buf[i]  >> shift) & 1;
                if(value == 1)
                {
                    highCount++ ;
                }
                else
                {
                    lowCount++;
                }
            }
            i++;
        }

        if(highCount > lowCount)
        {
            result[index] = 1;
        }
        else
        {
            result[index] = 0;
        }
        index ++;
    }

    return true;
}

static int searchPattern(uint8_t* buf , uint32_t bufSize , uint8_t* pattern , uint32_t patternSize)
{
    uint32_t j = 0;
    uint32_t i =0;
    uint32_t end = bufSize -1 ;
    for(i = 0  ; i <= end ; i ++)
    {
        uint8_t c = *(buf+i);
        if( (j==0) && (end - i +1 < patternSize) )
        {
            return -1;
        }
        if(c == pattern[j])
        {
            if( j == patternSize-1)
            {
                return i - patternSize+1;
            }
            j++;
        }
        else
        {
            j = 0 ;
            if(c == pattern[j])
            {
                j++;
            }
        }
    }
    return -1;
}

void SetupRFID()
{
    bufSize = SPI_MSG_LENGTH*8/32 ;
    buf = ICall_malloc(bufSize);
    result = ICall_malloc(RESULT_SIZE);
    resultLen = 0 ;

    Semaphore_Params semParams;
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    Semaphore_construct(&semStruct, 1, &semParams);
    semHandle = Semaphore_handle(&semStruct);
    Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

    SPI_Params spiParams;
    SPI_Params_init(&spiParams);
    spiParams.frameFormat = SPI_POL0_PHA1;
    spiParams.mode = SPI_SLAVE;
    spiParams.transferCallbackFxn = transferCompleteFxn;
    spiParams.transferMode = SPI_MODE_CALLBACK;
    slaveSpi = SPI_open(CONFIG_SPI_RFID, &spiParams);
    if (slaveSpi == NULL)
    {
        VVTK_LOGE("Error initializing slave SPI");
        while (1);
    }
    else
    {
        VVTK_LOGI( "Slave SPI initialized");
    }
}

bool LoopRFID()
{
    memset(result , 0 , RESULT_SIZE);
    resultLen = 0 ;
    SPI_Transaction transaction;
    bool transferOK;
    memset((void *) slaveRxBuffer, 0, SPI_MSG_LENGTH);
    transaction.count = SPI_MSG_LENGTH;
    transaction.txBuf = NULL;
    transaction.rxBuf = (void *) slaveRxBuffer;

    transferOK = SPI_transfer(slaveSpi, &transaction);
    if (!transferOK)
    {
        VVTK_LOGW( "SPI_transfer failed");
        return false;
    }

    Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);
    memset(buf,0 , bufSize);
    if(!sampleData( RF32, slaveRxBuffer , SPI_MSG_LENGTH ,buf , bufSize ))
    {
        VVTK_LOGW( "sampleData error" );
        return false;
    }

    uint8_t pattern[18]={1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0};
    int start  = searchPattern(buf , bufSize  , pattern , 18) ;
    if(start != -1 && start + 127 < bufSize)
    {
        if(!ManchesterDecode(buf + start , 128 , result , 64 ))
        {
            VVTK_LOGW("manchester decode error" );
            return false;
        }
        if(!CheckRowParityBit(result+9 , 11,5) || !CheckColumnParityBit(result+9 , 11,5))
        {
            VVTK_LOGW("check parity bit failed" );
            return false;
        }

        // for(i = 19 ; i < 59 ; i = i+5)
        // {
        //     char tmp[16]={0};
        //     for(j = 0 ; j < 4 ; j ++)
        //     {
        //         if(result[i+j] == 1)
        //         {
        //             strcat(tmp , "1");
        //         }
        //         else
        //         {
        //             strcat(tmp , "0");
        //         }
        //     }

        //     VVTK_LOGI(  "%s" , tmp);
        // }

//        VVTK_LOGI( "-------------------------------------------------------------------------");
        resultLen = 8 ;
        return true;
    }
    return false;
}

void *Task_rfid(void *arg0)
{
    SetupRFID();
    while(1)
    {
        LoopRFID();
    }
    SPI_close(slaveSpi);
    return (NULL);
}

void GetRFIDResult(uint8_t* data , uint8_t* len)
{
    if(data == NULL || resultLen <=0 )
    {
        *len = 0 ;
        return ;
    }

    memset(data , 0 , *len);
    *len = resultLen;
    int pos = 0 ;
    int i , j ;

    for(i = 19 ; i < 59 ; i = i+5)
    {
        uint8_t value = 8 ;
        for(j = 0 ; j < 4 ; j ++)
        {
            if(result[i+j] == 1)
            {
                data[pos] =  data[pos]  + value ;
            }
            value = value/2;
        }
        pos = pos +1;
    }

    char out[resultLen*2+3];
    memset(out , 0 ,resultLen*2+3 );
    Uint8ArrayToHexStr(data ,  *len  , out);
    VVTK_LOGI("rfid data : %s"  , out);
    VVTK_LOGI( "-------------------------------------------------------------------------");
}
