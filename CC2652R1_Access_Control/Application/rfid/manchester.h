#ifndef VVTK_MANCHESTER_H
#define VVTK_MANCHESTER_H

bool ManchesterDecode(uint8_t *buf , uint32_t bufSize , uint8_t *resut , uint32_t resultSize);
bool CheckRowParityBit(uint8_t *buf , int row , int column);
bool CheckColumnParityBit(uint8_t *buf , int row , int column);

#endif