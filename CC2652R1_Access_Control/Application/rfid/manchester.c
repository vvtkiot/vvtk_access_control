#include "Application/system/log.h"
#include "Application/tool/tool.h"

bool ManchesterDecode(uint8_t *buf , uint32_t bufSize , uint8_t *result , uint32_t resultSize)
{
    if(bufSize/2 > resultSize || bufSize%2 != 0)
    {
        return false;
    }
    int i = 0 ;
    int j = 0 ;
    int8_t preBit = -1;
    for(i = 0 ; i < bufSize ; i++)
    {
        if(preBit == -1)
        {
            preBit = buf[i];
        }
        else
        {
            if(preBit == 0 && buf[i] == 1)
            {
                result[j] = 0 ;
                j++ ;
            }
            else if(preBit == 1 && buf[i] == 0)
            {
                result[j] = 1 ;
                j++ ;
            }
            else
            {
                VVTK_LOGW("bit error");
                return false;
            }
            preBit = -1;
        }
    }

    return true;
}

bool CheckRowParityBit(uint8_t *buf , int row , int column)
{
    int i , j ;
    for(i = 0 ; i < row -1; i ++)
    {
        uint8_t sum = 0 ;
        for(j=0 ; j<column-1 ; j ++)
        {
            sum = sum + buf[i * column +j];
        }
        if(buf[i * column + column-1]!= sum%2)
        {
            return false;
        }
    }
    return true ;
}

bool CheckColumnParityBit(uint8_t *buf , int row , int column)
{
    int i,j;
    for(j=0 ; j<column-1 ; j++)
    {
        uint8_t sum = 0 ;
        for(i = 0; i < row-1 ; i++)
        {
            sum = sum +  buf[i * column +j];
        }
        if(buf[(row-1) * column +j]!=sum%2)
        {
            return false;
        }
    }
    return true;
}