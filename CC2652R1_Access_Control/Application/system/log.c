
// #include <unist.h>
// #include <stdint.h>
// #include <stddef.h>
#include <string.h>
#include <ti/display/DisplayUart.h>

#include "Application/system/log.h"

static Display_Handle display;

static char* string_replace(char* source, size_t sourceSize, char* substring, char* with)
{
    char* substring_source = strstr(source, substring);
    if (substring_source == NULL) {
        return NULL;
    }

    if (sourceSize < strlen(source) + (strlen(with) - strlen(substring)) + 1) {
        return NULL;
    }

    memmove(
        substring_source + strlen(with),
        substring_source + strlen(substring),
        strlen(substring_source) - strlen(substring) + 1
    );

    memcpy(substring_source, with, strlen(with));
    return substring_source + strlen(with);
}

void Log_init()
{
    Display_init();
    display = Display_open(Display_Type_ANY, NULL);
    if (display == NULL)
    {
        while (1);
    }
}

void Log_deinit()
{
    Display_close(display);
}

void VVTK_printf(char *fmt , ...)
{
    va_list va;
    va_start(va, fmt);

    DisplayUart_Object  *object  = (DisplayUart_Object  *)display->object;
    DisplayUart_HWAttrs *hwAttrs = (DisplayUart_HWAttrs *)display->hwAttrs;

    if (SemaphoreP_pend(object->mutex, hwAttrs->mutexTimeout) == SemaphoreP_OK)
    {
        SystemP_vsnprintf(hwAttrs->strBuf, hwAttrs->strBufLen, fmt, va);
        while(string_replace( hwAttrs->strBuf ,  hwAttrs->strBufLen , "\n" , "\r\n"));
        uint32_t strSize = strlen(hwAttrs->strBuf);
        UART_write(object->hUart, hwAttrs->strBuf, strSize);
        SemaphoreP_post(object->mutex);
    }

    va_end(va);
}

Display_Handle GetDisPlayHandle()
{
    return display  ;
}
