#ifndef VVTK_LOG_H
#define VVTK_LOG_H

#include <ti/display/AnsiColor.h>
#include <ti/display/Display.h>

// #define PRINTF VVTK_printf
#define PRINTF(fmt ,...) Display_printf(GetDisPlayHandle(), 0xFF, 0, fmt, ##__VA_ARGS__)
#define VVTK_LOGI(fmt , ...) {Display_printf(GetDisPlayHandle(), 0xFF, 0, ANSI_COLOR(FG_GREEN, ATTR_BOLD) fmt ANSI_COLOR(ATTR_RESET)  , ##__VA_ARGS__);}
#define VVTK_LOGW(fmt , ...) {Display_printf(GetDisPlayHandle(), 0xFF, 0, ANSI_COLOR(FG_YELLOW, ATTR_BOLD) fmt ANSI_COLOR(ATTR_RESET)  , ##__VA_ARGS__);}
#define VVTK_LOGE(fmt , ...) {Display_printf(GetDisPlayHandle(), 0xFF, 0, ANSI_COLOR(FG_RED, ATTR_BOLD) fmt ANSI_COLOR(ATTR_RESET)  , ##__VA_ARGS__);}

void Log_init();
void Log_deinit();
void VVTK_printf(char *fmt , ...);
Display_Handle GetDisPlayHandle();

#endif
