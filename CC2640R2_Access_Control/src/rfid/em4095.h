#ifndef VVTK_EM4095_H
#define VVTK_EM4095_H

typedef enum  RFType{
    RF32 = 32,
    RF64 = 64
} RFType;

void *Task_rfid(void *arg0);
void SetupRFID();
bool LoopRFID();
void GetRFIDResult(uint8_t* data , uint8_t* len);

#endif