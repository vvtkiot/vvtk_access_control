#ifndef VVTK_PERIPHERAL_H
#define VVTK_PERIPHERAL_H

#ifdef __cplusplus
extern "C"
{
#endif

void InitPeripheral();
void Peripheral_processStackMsg(void *pMsg);
// #ifdef VVTK_NFC
// void Peripheral_setNFCCharValue(void *value , uint8 len);
// #endif
#ifdef VVTK_RFID
void Peripheral_setRFIDCharValue(void *value , uint8 len);
#endif
// #ifdef VVTK_BLE_OBSERVER
// void Peripheral_setBLECharValue(void *value , uint8 len);
// #endif
#if defined(BLE_V42_FEATURES) && (BLE_V42_FEATURES & PRIVACY_1_2_CFG)
void Peripheral_updateRPA(void);
void Peripheral_processParamUpdate(uint16_t connHandle);
#endif

#ifdef __cplusplus
}
#endif

#endif
