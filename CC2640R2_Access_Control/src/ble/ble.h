#ifndef VVTK_BLE_H
#define VVTK_BLE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <gap.h>

// Application events
#define SP_STATE_CHANGE_EVT                  0
#define SP_ADV_EVT                           1
#define SP_PAIR_STATE_EVT                    2
#define SP_PASSCODE_EVT                      3
#define SP_PERIODIC_EVT                      4
#define SP_READ_RPA_EVT                      5
#define SP_SEND_PARAM_UPDATE_EVT             6

#define SC_EVT_SCAN_ENABLED        9
#define SC_EVT_SCAN_DISABLED       10
#define SC_EVT_ADV_REPORT          11
#define SC_EVT_INSUFFICIENT_MEM    17

// Container to store advertising event data when passing from advertising
// callback to app event. See the respective event in GapAdvScan_Event_IDs
// in gap_advertiser.h for the type that pBuf should be cast to.
typedef struct
{
  uint32_t event;
  void *pBuf;
} spGapAdvEventData_t;

// Container to store passcode data when passing from gapbondmgr callback
// to app event. See the pfnPasscodeCB_t documentation from the gapbondmgr.h
// header file for more information on each parameter.
typedef struct
{
  uint8_t deviceAddr[B_ADDR_LEN];
  uint16_t connHandle;
  uint8_t uiInputs;
  uint8_t uiOutputs;
  uint32_t numComparison;
} spPasscodeData_t;

// Container to store passcode data when passing from gapbondmgr callback
// to app event. See the pfnPairStateCB_t documentation from the gapbondmgr.h
// header file for more information on each parameter.
typedef struct
{
  uint8_t state;
  uint16_t connHandle;
  uint8_t status;
} spPairStateData_t;

// App event passed from stack modules. This type is defined by the application
// since it can queue events to itself however it wants.
typedef struct
{
  uint8_t event;                // event type
  void    *pData;               // pointer to message
} spEvt_t;

// Container to store information from clock expiration using a flexible array
// since data is not always needed
typedef struct
{
  uint8_t event;
  uint8_t data[];
} spClockEventData_t;

void CreateBleTask(void);
status_t BLE_EnqueueMsg(uint8_t event, void *pData);

// #ifdef VVTK_NFC
// void SetNFCCharValue(void *value , uint8 len);
// #endif
#ifdef VVTK_RFID
void SetRFIDCharValue(void *value , uint8 len);
#endif
// #ifdef VVTK_BLE_OBSERVER
// void SetBLECharValue(void *value , uint8 len);
// #endif

#ifdef __cplusplus
}
#endif

#endif
