#if !(defined __TI_COMPILER_VERSION__)
#include <intrinsics.h>
#endif

#include <ti/drivers/utils/List.h>
#include <icall.h>
#include <bcomdef.h>
/* This Header file contains all BLE API and icall structure definition */
#include <icall_ble_api.h>
#include "src/ble/PROFILES/devinfoservice.h"
#include "src/ble/PROFILES/gatt_profile.h"
#include "src/tool/util.h"
#include "src/ble/ble.h"
#include "src/system/log.h"

// Spin if the expression is not true
#define SIMPLEPERIPHERAL_ASSERT(expr) if (!(expr)) peripheral_spin();
// Address mode of the local device
// Note: When using the DEFAULT_ADDRESS_MODE as ADDRMODE_RANDOM or
// ADDRMODE_RP_WITH_RANDOM_ID, GAP_DeviceInit() should be called with
// it's last parameter set to a static random address
#define DEFAULT_ADDRESS_MODE                  ADDRMODE_PUBLIC
// General discoverable mode: advertise indefinitely
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL
// Minimum connection interval (units of 1.25ms, 80=100ms) for parameter update request
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     80
// Maximum connection interval (units of 1.25ms, 104=130ms) for  parameter update request
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     104
// Pass parameter updates to the app for it to decide.
#define DEFAULT_PARAM_UPDATE_REQ_DECISION     GAP_UPDATE_REQ_PASS_TO_APP
// Slave latency to use for parameter update request
#define DEFAULT_DESIRED_SLAVE_LATENCY         0
// Supervision timeout value (units of 10ms, 300=3s) for parameter update request
#define DEFAULT_DESIRED_CONN_TIMEOUT          300
// Delay (in ms) after connection establishment before sending a parameter update requst
#define SEND_PARAM_UPDATE_DELAY           6000
// How often to read current current RPA (in ms)
#define READ_RPA_EVT_PERIOD               3000

// For storing the active connections
#define RSSI_TRACK_CHNLS        1            // Max possible channels can be GAP_BONDINGS_MAX
#define MAX_RSSI_STORE_DEPTH    5
#define INVALID_HANDLE          0xFFFF
#define RSSI_2M_THRSHLD           -30
#define RSSI_1M_THRSHLD           -40
#define RSSI_S2_THRSHLD           -50
#define RSSI_S8_THRSHLD           -60
#define SP_PHY_NONE                LL_PHY_NONE  // No PHY set
#define AUTO_PHY_UPDATE            0xFF

// List element for parameter update and PHY command status lists
typedef struct
{
  List_Elem elem;
  uint16_t  connHandle;
} spConnHandleEntry_t;

// Connected device information
typedef struct
{
  uint16_t         	    connHandle;                        // Connection Handle
  spClockEventData_t*   pParamUpdateEventData;
  Clock_Struct*    	    pUpdateClock;                      // pointer to clock struct
  int8_t           	    rssiArr[MAX_RSSI_STORE_DEPTH];
  uint8_t          	    rssiCntr;
  int8_t           	    rssiAvg;
  bool             	    phyCngRq;                          // Set to true if PHY change request is in progress
  uint8_t          	    currPhy;
  uint8_t          	    rqPhy;
  uint8_t          	    phyRqFailCnt;                      // PHY change request count
  bool             	    isAutoPHYEnable;                   // Flag to indicate auto phy change
} spConnRec_t;

// Per-handle connection info
static spConnRec_t connList[MAX_NUM_BLE_CONNS];

// List to store connection handles for set phy command status's
static List_List setPhyCommStatList;

// List to store connection handles for queued param updates
static List_List paramUpdateList;

// Clock instance for RPA read events.
static Clock_Struct clkRpaRead;

// Memory to pass RPA read event ID to clock handler
spClockEventData_t argRpaRead =
{ .event = SP_READ_RPA_EVT };

// GAP GATT Attributes
static uint8_t attDeviceName[GAP_DEVICE_NAME_LEN] = "VVTK_Access_Control";

// Advertisement data
static uint8_t advertData[] =
{
  0x02,   // length of this data
  GAP_ADTYPE_FLAGS,
  DEFAULT_DISCOVERABLE_MODE | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

  // service UUID, to notify central devices what services are included
  // in this peripheral
  0x03,   // length of this data
  GAP_ADTYPE_16BIT_MORE,      // some of the UUID's, but not all
  LO_UINT16(ACCESS_CONTROL_SERV_UUID),
  HI_UINT16(ACCESS_CONTROL_SERV_UUID)
};

// Scan Response Data
static uint8_t scanRspData[] =
{
  // complete name
  20,   // length of this data
  GAP_ADTYPE_LOCAL_NAME_COMPLETE,
  'V',
  'V',
  'T',
  'K',
  '_',
  'A' ,
  'c',
  'c',
  'e',
  's',
  's',
  '_',
  'C',
  'o',
  'n',
  't',
  'r',
  'o',
  'l' ,
  // connection interval range
  5,   // length of this data
  GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE,
  LO_UINT16(DEFAULT_DESIRED_MIN_CONN_INTERVAL),   // 100ms
  HI_UINT16(DEFAULT_DESIRED_MIN_CONN_INTERVAL),
  LO_UINT16(DEFAULT_DESIRED_MAX_CONN_INTERVAL),   // 1s
  HI_UINT16(DEFAULT_DESIRED_MAX_CONN_INTERVAL),

  // Tx power level
  2,   // length of this data
  GAP_ADTYPE_POWER_LEVEL,
  0       // 0dBm
};

// Advertising handles
static uint8 advHandleLegacy;
static uint8 advHandleLongRange;

// Address mode
static GAP_Addr_Modes_t addrMode = DEFAULT_ADDRESS_MODE;

#if defined(BLE_V42_FEATURES) && (BLE_V42_FEATURES & PRIVACY_1_2_CFG)
// Current Random Private Address
static uint8 rpa[B_ADDR_LEN] = {0};
#endif // PRIVACY_1_2_CFG

#if defined(GAP_BOND_MGR)
static void peripheral_pairStateCb(uint16_t connHandle, uint8_t state, uint8_t status)
{
    spPairStateData_t *pData = ICall_malloc(sizeof(spPairStateData_t));

    // Allocate space for the event data.
    if (pData)
    {
        pData->state = state;
        pData->connHandle = connHandle;
        pData->status = status;

        // Queue the event.
        if(BLE_EnqueueMsg(SP_PAIR_STATE_EVT, pData) != SUCCESS)
        {
            ICall_free(pData);
        }
    }
}

static void peripheral_passcodeCb(uint8_t *pDeviceAddr,
                                        uint16_t connHandle,
                                        uint8_t uiInputs,
                                        uint8_t uiOutputs,
                                        uint32_t numComparison)
{
    spPasscodeData_t *pData = ICall_malloc(sizeof(spPasscodeData_t));

    // Allocate space for the passcode event.
    if (pData )
    {
        pData->connHandle = connHandle;
        memcpy(pData->deviceAddr, pDeviceAddr, B_ADDR_LEN);
        pData->uiInputs = uiInputs;
        pData->uiOutputs = uiOutputs;
        pData->numComparison = numComparison;

        // Enqueue the event.
        if(BLE_EnqueueMsg(SP_PASSCODE_EVT, pData) != SUCCESS)
        {
            ICall_free(pData);
        }
    }
}

// GAP Bond Manager Callbacks
static gapBondCBs_t peripheral_BondMgrCBs =
{
  peripheral_passcodeCb,       // Passcode callback
  peripheral_pairStateCb       // Pairing/Bonding state Callback
};
#endif

static void peripheral_spin(void)
{
    volatile uint8_t x = 0;

    while(1)
    {
        x++;
    }
}

static status_t peripheral_stopAutoPhyChange(uint16_t connHandle)
{
    // Get connection index from handle
    uint8_t connIndex = peripheral_getConnIndex(connHandle);
    SIMPLEPERIPHERAL_ASSERT(connIndex < MAX_NUM_BLE_CONNS);

    // Stop connection event notice
    Gap_RegisterConnEventCb(NULL, GAP_CB_UNREGISTER, connHandle);

    // Also update the phychange request status for active RSSI tracking connection
    connList[connIndex].phyCngRq = FALSE;
    connList[connIndex].isAutoPHYEnable = FALSE;

    return SUCCESS;
}

void peripheral_clearPendingParamUpdate(uint16_t connHandle)
{
    List_Elem *curr;

    for (curr = List_head(&paramUpdateList); curr != NULL; curr = List_next(curr))
    {
        if (((spConnHandleEntry_t *)curr)->connHandle == connHandle)
        {
            List_remove(&paramUpdateList, curr);
        }
    }
}

static uint8_t peripheral_getConnIndex(uint16_t connHandle)
{
    uint8_t i;

    for (i = 0; i < MAX_NUM_BLE_CONNS; i++)
    {
        if (connList[i].connHandle == connHandle)
        {
            return i;
        }
    }

    return(MAX_NUM_BLE_CONNS);
}

static uint8_t peripheral_clearConnListEntry(uint16_t connHandle)
{
    uint8_t i;
    // Set to invalid connection index initially
    uint8_t connIndex = MAX_NUM_BLE_CONNS;

    if(connHandle != CONNHANDLE_ALL)
    {
        // Get connection index from handle
        connIndex = peripheral_getConnIndex(connHandle);
        if(connIndex >= MAX_NUM_BLE_CONNS)
        {
            return(bleInvalidRange);
        }
    }

    // Clear specific handle or all handles
    for(i = 0; i < MAX_NUM_BLE_CONNS; i++)
    {
        if((connIndex == i) || (connHandle == CONNHANDLE_ALL))
        {
            connList[i].connHandle = CONNHANDLE_INVALID;
            connList[i].currPhy = 0;
            connList[i].phyCngRq = 0;
            connList[i].phyRqFailCnt = 0;
            connList[i].rqPhy = 0;
            memset(connList[i].rssiArr, 0, MAX_RSSI_STORE_DEPTH);
            connList[i].rssiAvg = 0;
            connList[i].rssiCntr = 0;
            connList[i].isAutoPHYEnable = FALSE;
        }
    }

    return(SUCCESS);
}

static void peripheral_initPHYRSSIArray(void)
{
    //Initialize array to store connection handle and RSSI values
    memset(connList, 0, sizeof(connList));
    for (uint8_t index = 0; index < MAX_NUM_BLE_CONNS; index++)
    {
        connList[index].connHandle = INVALID_HANDLE;
    }
}

void InitPeripheral(ICall_EntityID *selfEntity )
{
    #ifdef USE_RCOSC
    RCOSC_enableCalibration();
    #endif // USE_RCOSC

    // Set the Device Name characteristic in the GAP GATT Service
    // For more information, see the section in the User's Guide:
    // http://software-dl.ti.com/lprf/ble5stack-latest/
    GGS_SetParameter(GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName);

    // Configure GAP
    {
        uint16_t paramUpdateDecision = DEFAULT_PARAM_UPDATE_REQ_DECISION;

        // Pass all parameter update requests to the app for it to decide
        GAP_SetParamValue(GAP_PARAM_LINK_UPDATE_DECISION, paramUpdateDecision);
    }

    #if defined(GAP_BOND_MGR)
    // Setup the GAP Bond Manager. For more information see the GAP Bond Manager
    // section in the User's Guide:
    // http://software-dl.ti.com/lprf/ble5stack-latest/
    {
        // Don't send a pairing request after connecting; the peer device must
        // initiate pairing
        uint8_t pairMode = GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
        // Use authenticated pairing: require passcode.
        uint8_t mitm = TRUE;
        // This device only has display capabilities. Therefore, it will display the
        // passcode during pairing. However, since the default passcode is being
        // used, there is no need to display anything.
        uint8_t ioCap = GAPBOND_IO_CAP_DISPLAY_ONLY;
        // Request bonding (storing long-term keys for re-encryption upon subsequent
        // connections without repairing)
        uint8_t bonding = TRUE;

        GAPBondMgr_SetParameter(GAPBOND_PAIRING_MODE, sizeof(uint8_t), &pairMode);
        GAPBondMgr_SetParameter(GAPBOND_MITM_PROTECTION, sizeof(uint8_t), &mitm);
        GAPBondMgr_SetParameter(GAPBOND_IO_CAPABILITIES, sizeof(uint8_t), &ioCap);
        GAPBondMgr_SetParameter(GAPBOND_BONDING_ENABLED, sizeof(uint8_t), &bonding);
    }
    #endif

    // Initialize GATT attributes
    GGS_AddService(GATT_ALL_SERVICES);           // GAP GATT Service
    GATTServApp_AddService(GATT_ALL_SERVICES);   // GATT Service
    DevInfo_AddService();                        // Device Information Service
    SimpleProfile_AddService(GATT_ALL_SERVICES); // Simple GATT Profile

    #if defined(GAP_BOND_MGR)
    // Start Bond Manager and register callback
    VOID GAPBondMgr_Register(&peripheral_BondMgrCBs);
    #endif

    // Register with GAP for HCI/Host messages. This is needed to receive HCI
    // events. For more information, see the HCI section in the User's Guide:
    // http://software-dl.ti.com/lprf/ble5stack-latest/
    GAP_RegisterForMsgs(*selfEntity);

    // Register for GATT local events and ATT Responses pending for transmission
    GATT_RegisterForMsgs(*selfEntity);

    // Set default values for Data Length Extension
    // Extended Data Length Feature is already enabled by default
    {
        // Set initial values to maximum, RX is set to max. by default(251 octets, 2120us)
        // Some brand smartphone is essentially needing 251/2120, so we set them here.
        #define APP_SUGGESTED_PDU_SIZE 251 //default is 27 octets(TX)
        #define APP_SUGGESTED_TX_TIME 2120 //default is 328us(TX)

        // This API is documented in hci.h
        // See the LE Data Length Extension section in the BLE5-Stack User's Guide for information on using this command:
        // http://software-dl.ti.com/lprf/ble5stack-latest/
        HCI_LE_WriteSuggestedDefaultDataLenCmd(APP_SUGGESTED_PDU_SIZE, APP_SUGGESTED_TX_TIME);
    }

    // Initialize GATT Client
    GATT_InitClient();

    // Initialize Connection List
    peripheral_clearConnListEntry(CONNHANDLE_ALL);

    //Initialize GAP layer for Peripheral role and register to receive GAP events
    GAP_DeviceInit(GAP_PROFILE_PERIPHERAL | GAP_PROFILE_OBSERVER, *selfEntity, addrMode, NULL);

    // Initialize array to store connection handle and RSSI values
    peripheral_initPHYRSSIArray();
}

#if defined(BLE_V42_FEATURES) && (BLE_V42_FEATURES & PRIVACY_1_2_CFG)
void Peripheral_updateRPA(void)
{
    uint8_t* pRpaNew;

    // Read the current RPA.
    pRpaNew = GAP_GetDevAddress(FALSE);

    if (memcmp(pRpaNew, rpa, B_ADDR_LEN))
    {
        // If the RPA has changed, update the display
        VVTK_LOGI("RP Addr: %s",Util_convertBdAddr2Str(pRpaNew));
        memcpy(rpa, pRpaNew, B_ADDR_LEN);
    }
}
#endif // PRIVACY_1_2_CFG

static void peripheral_clockHandler(UArg arg)
{
    spClockEventData_t *pData = (spClockEventData_t *)arg;
    if (pData->event == SP_READ_RPA_EVT)
    {
        // Start the next period
        Util_startClock(&clkRpaRead);

        // Post event to read the current RPA
        BLE_EnqueueMsg(SP_READ_RPA_EVT, NULL);
    }
    else if (pData->event == SP_SEND_PARAM_UPDATE_EVT)
    {
        // Send message to app
        BLE_EnqueueMsg(SP_SEND_PARAM_UPDATE_EVT, pData);
    }
}

static uint8_t peripheral_addConn(uint16_t connHandle)
{
    uint8_t i;
    uint8_t status = bleNoResources;

    // Try to find an available entry
    for (i = 0; i < MAX_NUM_BLE_CONNS; i++)
    {
        if (connList[i].connHandle == CONNHANDLE_INVALID)
        {
        // Found available entry to put a new connection info in
        connList[i].connHandle = connHandle;

        // Allocate data to send through clock handler
        connList[i].pParamUpdateEventData = ICall_malloc(sizeof(spClockEventData_t) +
                                                        sizeof (uint16_t));
        if(connList[i].pParamUpdateEventData)
        {
            connList[i].pParamUpdateEventData->event = SP_SEND_PARAM_UPDATE_EVT;
            *((uint16_t *)connList[i].pParamUpdateEventData->data) = connHandle;

            // Create a clock object and start
            connList[i].pUpdateClock
            = (Clock_Struct*) ICall_malloc(sizeof(Clock_Struct));

            if (connList[i].pUpdateClock)
            {
                Util_constructClock(connList[i].pUpdateClock,
                                    peripheral_clockHandler,
                                    SEND_PARAM_UPDATE_DELAY, 0, true,
                                    (UArg) (connList[i].pParamUpdateEventData));
            }
            else
            {
                ICall_free(connList[i].pParamUpdateEventData);
            }
        }
        else
        {
            status = bleMemAllocError;
        }

        // Set default PHY to 1M
        connList[i].currPhy = HCI_PHY_1_MBPS;

        break;
        }
    }

    return status;
}

static uint8_t peripheral_removeConn(uint16_t connHandle)
{
    uint8_t connIndex = peripheral_getConnIndex(connHandle);

    if(connIndex != MAX_NUM_BLE_CONNS)
    {
        Clock_Struct* pUpdateClock = connList[connIndex].pUpdateClock;

        if (pUpdateClock != NULL)
        {
            // Stop and destruct the RTOS clock if it's still alive
            if (Util_isActive(pUpdateClock))
            {
                Util_stopClock(pUpdateClock);
            }

            // Destruct the clock object
            Clock_destruct(pUpdateClock);
            // Free clock struct
            ICall_free(pUpdateClock);
            // Free ParamUpdateEventData
            ICall_free(connList[connIndex].pParamUpdateEventData);
        }
        // Clear pending update requests from paramUpdateList
        peripheral_clearPendingParamUpdate(connHandle);
        // Stop Auto PHY Change
        peripheral_stopAutoPhyChange(connHandle);
        // Clear Connection List Entry
        peripheral_clearConnListEntry(connHandle);
    }

    return connIndex;
}

void Peripheral_processParamUpdate(uint16_t connHandle)
{
    gapUpdateLinkParamReq_t req;
    uint8_t connIndex;

    req.connectionHandle = connHandle;
    req.connLatency = DEFAULT_DESIRED_SLAVE_LATENCY;
    req.connTimeout = DEFAULT_DESIRED_CONN_TIMEOUT;
    req.intervalMin = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
    req.intervalMax = DEFAULT_DESIRED_MAX_CONN_INTERVAL;

    connIndex = peripheral_getConnIndex(connHandle);
    if (connIndex >= MAX_NUM_BLE_CONNS)
    {
        VVTK_LOGW( "Connection handle is not in the connList !!!");
        return;
    }

    // Deconstruct the clock object
    Clock_destruct(connList[connIndex].pUpdateClock);
    // Free clock struct, only in case it is not NULL
    if (connList[connIndex].pUpdateClock != NULL)
    {
        ICall_free(connList[connIndex].pUpdateClock);
        connList[connIndex].pUpdateClock = NULL;
    }
    // Free ParamUpdateEventData, only in case it is not NULL
    if (connList[connIndex].pParamUpdateEventData != NULL)
        ICall_free(connList[connIndex].pParamUpdateEventData);

    // Send parameter update
    bStatus_t status = GAP_UpdateLinkParamReq(&req);

    // If there is an ongoing update, queue this for when the udpate completes
    if (status == bleAlreadyInRequestedMode)
    {
        spConnHandleEntry_t *connHandleEntry = ICall_malloc(sizeof(spConnHandleEntry_t));
        if (connHandleEntry)
        {
            connHandleEntry->connHandle = connHandle;

            List_put(&paramUpdateList, (List_Elem *)connHandleEntry);
        }
    }
}

static void peripheral_advCallback(uint32_t event, void *pBuf, uintptr_t arg)
{
    spGapAdvEventData_t *pData = ICall_malloc(sizeof(spGapAdvEventData_t));

    if (pData)
    {
        pData->event = event;
        pData->pBuf = pBuf;

        if(BLE_EnqueueMsg(SP_ADV_EVT, pData) != SUCCESS)
        {
            ICall_free(pData);
        }
    }
}

static void peripheral_processGapMessage(gapEventHdr_t *pMsg)
{
    switch(pMsg->opcode)
    {
        case GAP_DEVICE_INIT_DONE_EVENT:
        {
            bStatus_t status = FAILURE;
            gapDeviceInitDoneEvent_t *pPkt = (gapDeviceInitDoneEvent_t *)pMsg;

            if(pPkt->hdr.status == SUCCESS)
            {
                // Store the system ID
                uint8_t systemId[DEVINFO_SYSTEM_ID_LEN];

                // use 6 bytes of device address for 8 bytes of system ID value
                systemId[0] = pPkt->devAddr[0];
                systemId[1] = pPkt->devAddr[1];
                systemId[2] = pPkt->devAddr[2];

                // set middle bytes to zero
                systemId[4] = 0x00;
                systemId[3] = 0x00;

                // shift three bytes up
                systemId[7] = pPkt->devAddr[5];
                systemId[6] = pPkt->devAddr[4];
                systemId[5] = pPkt->devAddr[3];

                // Set Device Info Service Parameter
                DevInfo_SetParameter(DEVINFO_SYSTEM_ID, DEVINFO_SYSTEM_ID_LEN, systemId);

                VVTK_LOGI("Initialized");

                // Setup and start Advertising
                // For more information, see the GAP section in the User's Guide:
                // http://software-dl.ti.com/lprf/ble5stack-latest/

                // Temporary memory for advertising parameters for set #1. These will be copied
                // by the GapAdv module
                GapAdv_params_t advParamLegacy = GAPADV_PARAMS_LEGACY_SCANN_CONN;

                // Create Advertisement set #1 and assign handle
                status = GapAdv_create(&peripheral_advCallback, &advParamLegacy,
                                    &advHandleLegacy);
                SIMPLEPERIPHERAL_ASSERT(status == SUCCESS);

                // Load advertising data for set #1 that is statically allocated by the app
                status = GapAdv_loadByHandle(advHandleLegacy, GAP_ADV_DATA_TYPE_ADV,
                                            sizeof(advertData), advertData);
                SIMPLEPERIPHERAL_ASSERT(status == SUCCESS);

                // Load scan response data for set #1 that is statically allocated by the app
                status = GapAdv_loadByHandle(advHandleLegacy, GAP_ADV_DATA_TYPE_SCAN_RSP,
                                            sizeof(scanRspData), scanRspData);
                SIMPLEPERIPHERAL_ASSERT(status == SUCCESS);

                // Set event mask for set #1
                status = GapAdv_setEventMask(advHandleLegacy,
                                            GAP_ADV_EVT_MASK_START_AFTER_ENABLE |
                                            GAP_ADV_EVT_MASK_END_AFTER_DISABLE |
                                            GAP_ADV_EVT_MASK_SET_TERMINATED);

                // Enable legacy advertising for set #1
                status = GapAdv_enable(advHandleLegacy, GAP_ADV_ENABLE_OPTIONS_USE_MAX , 0);
                SIMPLEPERIPHERAL_ASSERT(status == SUCCESS);

                // Use long range params to create long range set #2
                GapAdv_params_t advParamLongRange = GAPADV_PARAMS_AE_LONG_RANGE_CONN;

                // Create Advertisement set #2 and assign handle
                status = GapAdv_create(&peripheral_advCallback, &advParamLongRange,
                                    &advHandleLongRange);
                SIMPLEPERIPHERAL_ASSERT(status == SUCCESS);

                // Load advertising data for set #2 that is statically allocated by the app
                status = GapAdv_loadByHandle(advHandleLongRange, GAP_ADV_DATA_TYPE_ADV,
                                            sizeof(advertData), advertData);
                SIMPLEPERIPHERAL_ASSERT(status == SUCCESS);

                // Set event mask for set #2
                status = GapAdv_setEventMask(advHandleLongRange,
                                            GAP_ADV_EVT_MASK_START_AFTER_ENABLE |
                                            GAP_ADV_EVT_MASK_END_AFTER_DISABLE |
                                            GAP_ADV_EVT_MASK_SET_TERMINATED);

                // Enable long range advertising for set #2
                status = GapAdv_enable(advHandleLongRange, GAP_ADV_ENABLE_OPTIONS_USE_MAX , 0);
                SIMPLEPERIPHERAL_ASSERT(status == SUCCESS);

                // Display device address
                VVTK_LOGI("%s Addr: %s",
                            (addrMode <= ADDRMODE_RANDOM) ? "Dev" : "ID",
                            Util_convertBdAddr2Str(pPkt->devAddr));

        #if defined(BLE_V42_FEATURES) && (BLE_V42_FEATURES & PRIVACY_1_2_CFG)
                if (addrMode > ADDRMODE_RANDOM)
                {
                    Peripheral_updateRPA();

                    // Create one-shot clock for RPA check event.
                    Util_constructClock(&clkRpaRead, peripheral_clockHandler,
                                        READ_RPA_EVT_PERIOD, 0, true,
                                        (UArg) &argRpaRead);
                }
        #endif // PRIVACY_1_2_CFG
            }
            break;
        }

        case GAP_LINK_ESTABLISHED_EVENT:
        {
            gapEstLinkReqEvent_t *pPkt = (gapEstLinkReqEvent_t *)pMsg;

            // Display the amount of current connections
            uint8_t numActive = linkDB_NumActive();
            VVTK_LOGI("Num Conns: %d",(uint16_t)numActive);

            if (pPkt->hdr.status == SUCCESS)
            {
                // Add connection to list and start RSSI
                peripheral_addConn(pPkt->connectionHandle);

                // Display the address of this connection
                VVTK_LOGI("Connected to %s",Util_convertBdAddr2Str(pPkt->devAddr));
            }

            if (numActive < MAX_NUM_BLE_CONNS)
            {
                // Start advertising since there is room for more connections
                GapAdv_enable(advHandleLegacy, GAP_ADV_ENABLE_OPTIONS_USE_MAX , 0);
                GapAdv_enable(advHandleLongRange, GAP_ADV_ENABLE_OPTIONS_USE_MAX , 0);
            }
            else
            {
                // Stop advertising since there is no room for more connections
                GapAdv_disable(advHandleLongRange);
                GapAdv_disable(advHandleLegacy);
            }

            break;
        }

        case GAP_LINK_TERMINATED_EVENT:
        {
            gapTerminateLinkEvent_t *pPkt = (gapTerminateLinkEvent_t *)pMsg;

            // Display the amount of current connections
            uint8_t numActive = linkDB_NumActive();
            VVTK_LOGW("Device Disconnected!");
            VVTK_LOGI("Num Conns: %d",(uint16_t)numActive);

            // Remove the connection from the list and disable RSSI if needed
            peripheral_removeConn(pPkt->connectionHandle);

            // Start advertising since there is room for more connections
            GapAdv_enable(advHandleLegacy, GAP_ADV_ENABLE_OPTIONS_USE_MAX , 0);
            GapAdv_enable(advHandleLongRange, GAP_ADV_ENABLE_OPTIONS_USE_MAX , 0);

            break;
        }

        case GAP_UPDATE_LINK_PARAM_REQ_EVENT:
        {
            gapUpdateLinkParamReqReply_t rsp;

            gapUpdateLinkParamReqEvent_t *pReq = (gapUpdateLinkParamReqEvent_t *)pMsg;

            rsp.connectionHandle = pReq->req.connectionHandle;

            // Only accept connection intervals with slave latency of 0
            // This is just an example of how the application can send a response
            if(pReq->req.connLatency == 0)
            {
                rsp.intervalMin = pReq->req.intervalMin;
                rsp.intervalMax = pReq->req.intervalMax;
                rsp.connLatency = pReq->req.connLatency;
                rsp.connTimeout = pReq->req.connTimeout;
                rsp.accepted = TRUE;
            }
            else
            {
                rsp.accepted = FALSE;
            }

            // Send Reply
            VOID GAP_UpdateLinkParamReqReply(&rsp);

            break;
        }

        case GAP_LINK_PARAM_UPDATE_EVENT:
        {
            gapLinkUpdateEvent_t *pPkt = (gapLinkUpdateEvent_t *)pMsg;

            // Get the address from the connection handle
            linkDBInfo_t linkInfo;
            linkDB_GetInfo(pPkt->connectionHandle, &linkInfo);

            if(pPkt->status == SUCCESS)
            {
                // Display the address of the connection update
                VVTK_LOGI("Link Param Updated: %s",Util_convertBdAddr2Str(linkInfo.addr));
            }
            else
            {
                // Display the address of the connection update failure
                VVTK_LOGI("Link Param Update Failed 0x%x: %s", pPkt->opcode,Util_convertBdAddr2Str(linkInfo.addr));
            }

            // Check if there are any queued parameter updates
            spConnHandleEntry_t *connHandleEntry = (spConnHandleEntry_t *)List_get(&paramUpdateList);
            if (connHandleEntry != NULL)
            {
                // Attempt to send queued update now
                Peripheral_processParamUpdate(connHandleEntry->connHandle);

                // Free list element
                ICall_free(connHandleEntry);
            }

            break;
        }
        default:
        break;
    }
}

static uint8_t peripheral_processGATTMsg(gattMsgEvent_t *pMsg)
{
    if (pMsg->method == ATT_FLOW_CTRL_VIOLATED_EVENT)
    {
        // ATT request-response or indication-confirmation flow control is
        // violated. All subsequent ATT requests or indications will be dropped.
        // The app is informed in case it wants to drop the connection.

        // Display the opcode of the message that caused the violation.
        VVTK_LOGW("FC Violated: %d", pMsg->msg.flowCtrlEvt.opcode);
    }
    else if (pMsg->method == ATT_MTU_UPDATED_EVENT)
    {
        // MTU size updated
        VVTK_LOGI("MTU Size: %d", pMsg->msg.mtuEvt.MTU);
    }

    // Free message payload. Needed only for ATT Protocol messages
    GATT_bm_free(&pMsg->msg, pMsg->method);

    // It's safe to free the incoming message
    return (TRUE);
}

static status_t peripheral_setPhy(uint16_t connHandle, uint8_t allPhys,
                                        uint8_t txPhy, uint8_t rxPhy,
                                        uint16_t phyOpts)
{
  // Allocate list entry to store handle for command status
  spConnHandleEntry_t *connHandleEntry = ICall_malloc(sizeof(spConnHandleEntry_t));

  if (connHandleEntry)
  {
    connHandleEntry->connHandle = connHandle;

    // Add entry to the phy command status list
    List_put(&setPhyCommStatList, (List_Elem *)connHandleEntry);

    // Send PHY Update
    HCI_LE_SetPhyCmd(connHandle, allPhys, txPhy, rxPhy, phyOpts);
  }

  return SUCCESS;
}


static void peripheral_processCmdCompleteEvt(hciEvt_CmdComplete_t *pMsg)
{
    uint8_t status = pMsg->pReturnParam[0];

    //Find which command this command complete is for
    switch (pMsg->cmdOpcode)
    {
        case HCI_READ_RSSI:
        {
            int8 rssi = (int8)pMsg->pReturnParam[3];

            // Display RSSI value, if RSSI is higher than threshold, change to faster PHY
            if (status == SUCCESS)
            {
                uint16_t handle = BUILD_UINT16(pMsg->pReturnParam[1], pMsg->pReturnParam[2]);

                uint8_t index = peripheral_getConnIndex(handle);
                SIMPLEPERIPHERAL_ASSERT(index < MAX_NUM_BLE_CONNS);

                if (rssi != LL_RSSI_NOT_AVAILABLE)
                {
                    connList[index].rssiArr[connList[index].rssiCntr++] = rssi;
                    connList[index].rssiCntr %= MAX_RSSI_STORE_DEPTH;

                    int16_t sum_rssi = 0;
                    for(uint8_t cnt=0; cnt<MAX_RSSI_STORE_DEPTH; cnt++)
                    {
                        sum_rssi += connList[index].rssiArr[cnt];
                    }
                    connList[index].rssiAvg = (uint32_t)(sum_rssi/MAX_RSSI_STORE_DEPTH);

                    uint8_t phyRq = SP_PHY_NONE;
                    uint8_t phyRqS = SP_PHY_NONE;
                    uint8_t phyOpt = LL_PHY_OPT_NONE;

                    if(connList[index].phyCngRq == FALSE)
                    {
                        if((connList[index].rssiAvg >= RSSI_2M_THRSHLD) &&
                        (connList[index].currPhy != HCI_PHY_2_MBPS) &&
                            (connList[index].currPhy != SP_PHY_NONE))
                        {
                            // try to go to higher data rate
                            phyRqS = phyRq = HCI_PHY_2_MBPS;
                        }
                        else if((connList[index].rssiAvg < RSSI_2M_THRSHLD) &&
                                (connList[index].rssiAvg >= RSSI_1M_THRSHLD) &&
                                (connList[index].currPhy != HCI_PHY_1_MBPS) &&
                                (connList[index].currPhy != SP_PHY_NONE))
                        {
                            // try to go to legacy regular data rate
                            phyRqS = phyRq = HCI_PHY_1_MBPS;
                        }
                        else if((connList[index].rssiAvg >= RSSI_S2_THRSHLD) &&
                                (connList[index].rssiAvg < RSSI_1M_THRSHLD) &&
                                (connList[index].currPhy != SP_PHY_NONE))
                        {
                            // try to go to lower data rate S=2(500kb/s)
                            phyRqS = HCI_PHY_CODED;
                            phyOpt = LL_PHY_OPT_S2;
                            phyRq = BLE5_CODED_S2_PHY;
                        }
                        else if(connList[index].rssiAvg < RSSI_S2_THRSHLD )
                        {
                            // try to go to lowest data rate S=8(125kb/s)
                            phyRqS = HCI_PHY_CODED;
                            phyOpt = LL_PHY_OPT_S8;
                            phyRq = BLE5_CODED_S8_PHY;
                        }
                        if((phyRq != SP_PHY_NONE) &&
                        // First check if the request for this phy change is already not honored then don't request for change
                        (((connList[index].rqPhy == phyRq) &&
                            (connList[index].phyRqFailCnt < 2)) ||
                            (connList[index].rqPhy != phyRq)))
                        {
                            //Initiate PHY change based on RSSI
                            peripheral_setPhy(connList[index].connHandle, 0,phyRqS, phyRqS, phyOpt);
                            connList[index].phyCngRq = TRUE;

                            // If it a request for different phy than failed request, reset the count
                            if(connList[index].rqPhy != phyRq)
                            {
                                // then reset the request phy counter and requested phy
                                connList[index].phyRqFailCnt = 0;
                            }

                            if(phyOpt == LL_PHY_OPT_NONE)
                            {
                                connList[index].rqPhy = phyRq;
                            }
                            else if(phyOpt == LL_PHY_OPT_S2)
                            {
                                connList[index].rqPhy = BLE5_CODED_S2_PHY;
                            }
                            else
                            {
                                connList[index].rqPhy = BLE5_CODED_S8_PHY;
                            }

                        } // end of if ((phyRq != SP_PHY_NONE) && ...
                    } // end of if (connList[index].phyCngRq == FALSE)
                } // end of if (rssi != LL_RSSI_NOT_AVAILABLE)

                VVTK_LOGI("RSSI:%d dBm, AVG RSSI:%d dBm",(uint32_t)(rssi),connList[index].rssiAvg);

            } // end of if (status == SUCCESS)
            break;
        }

        case HCI_LE_READ_PHY:
        {
            if (status == SUCCESS)
            {
                VVTK_LOGI("RXPh: %d, TXPh: %d",pMsg->pReturnParam[3], pMsg->pReturnParam[4]);
            }
            break;
        }

        default:
        break;
    } // end of switch (pMsg->cmdOpcode)
}

static void peripheral_updatePHYStat(uint16_t eventCode, uint8_t *pMsg)
{
    uint8_t connIndex;

    switch (eventCode)
    {
        case HCI_LE_SET_PHY:
        {
            // Get connection handle from list
            spConnHandleEntry_t *connHandleEntry = (spConnHandleEntry_t *)List_get(&setPhyCommStatList);
            if (connHandleEntry)
            {
                // Get index from connection handle
                connIndex = peripheral_getConnIndex(connHandleEntry->connHandle);

                ICall_free(connHandleEntry);

                // Is this connection still valid?
                if (connIndex < MAX_NUM_BLE_CONNS)
                {
                    hciEvt_CommandStatus_t *pMyMsg = (hciEvt_CommandStatus_t *)pMsg;

                    if (pMyMsg->cmdStatus == HCI_ERROR_CODE_UNSUPPORTED_REMOTE_FEATURE)
                    {
                        // Update the phychange request status for active RSSI tracking connection
                        connList[connIndex].phyCngRq = FALSE;
                        connList[connIndex].phyRqFailCnt++;
                    }
                }
            }
            break;
        }

        // LE Event - a Phy update has completed or failed
        case HCI_BLE_PHY_UPDATE_COMPLETE_EVENT:
        {
            hciEvt_BLEPhyUpdateComplete_t *pPUC = (hciEvt_BLEPhyUpdateComplete_t*) pMsg;

            if(pPUC)
            {
                // Get index from connection handle
                connIndex = peripheral_getConnIndex(pPUC->connHandle);

                // Is this connection still valid?
                if (connIndex < MAX_NUM_BLE_CONNS)
                {
                    // Update the phychange request status for active RSSI tracking connection
                    connList[connIndex].phyCngRq = FALSE;

                    if (pPUC->status == SUCCESS)
                    {
                        connList[connIndex].currPhy = pPUC->rxPhy;
                    }
                    if(pPUC->rxPhy != connList[connIndex].rqPhy)
                    {
                        connList[connIndex].phyRqFailCnt++;
                    }
                    else
                    {
                        // Reset the request phy counter and requested phy
                        connList[connIndex].phyRqFailCnt = 0;
                        connList[connIndex].rqPhy = 0;
                    }
                }
            }
            break;
        }

        default:
        break;
    } // end of switch (eventCode)
}

void Peripheral_processStackMsg(void *pMsg)
{
    switch (((ICall_Hdr *)pMsg)->event)
    {
        case GAP_MSG_EVENT:
            peripheral_processGapMessage((gapEventHdr_t*) pMsg);
            break;

        case GATT_MSG_EVENT:
            // Process GATT message
            peripheral_processGATTMsg((gattMsgEvent_t *)pMsg);
            break;

        case HCI_GAP_EVENT_EVENT:
        {
            // Process HCI message
            switch(((ICall_Hdr *)pMsg)->status)
            {
                case HCI_COMMAND_COMPLETE_EVENT_CODE:
                // Process HCI Command Complete Events here
                {
                    peripheral_processCmdCompleteEvt((hciEvt_CmdComplete_t *) pMsg);
                    break;
                }

                case HCI_BLE_HARDWARE_ERROR_EVENT_CODE:
                    AssertHandler(HAL_ASSERT_CAUSE_HARDWARE_ERROR,0);
                    break;

                // HCI Commands Events
                case HCI_COMMAND_STATUS_EVENT_CODE:
                {
                    hciEvt_CommandStatus_t *pMyMsg = (hciEvt_CommandStatus_t *)pMsg;
                    switch ( pMyMsg->cmdOpcode )
                    {
                        case HCI_LE_SET_PHY:
                        {
                            if (pMyMsg->cmdStatus == HCI_ERROR_CODE_UNSUPPORTED_REMOTE_FEATURE)
                            {
                                VVTK_LOGW("PHY Change failure, peer does not support this");
                            }
                            else
                            {
                                VVTK_LOGI("PHY Update Status Event: 0x%x",pMyMsg->cmdStatus);
                            }

                            peripheral_updatePHYStat(HCI_LE_SET_PHY, (uint8_t *)pMsg);
                            break;
                        }

                        default:
                        break;
                    }
                    break;
                }

                // LE Events
                case HCI_LE_EVENT_CODE:
                {
                    hciEvt_BLEPhyUpdateComplete_t *pPUC = (hciEvt_BLEPhyUpdateComplete_t*) pMsg;

                    // A Phy Update Has Completed or Failed
                    if (pPUC->BLEEventCode == HCI_BLE_PHY_UPDATE_COMPLETE_EVENT)
                    {
                        if (pPUC->status != SUCCESS)
                        {
                            VVTK_LOGW("PHY Change failure");
                        }
                        else
                        {
                            // Only symmetrical PHY is supported.
                            // rxPhy should be equal to txPhy.
                            VVTK_LOGI("PHY Updated to %s",
                                            (pPUC->rxPhy == PHY_UPDATE_COMPLETE_EVENT_1M) ? "1M" :
                                            (pPUC->rxPhy == PHY_UPDATE_COMPLETE_EVENT_2M) ? "2M" :
                                            (pPUC->rxPhy == PHY_UPDATE_COMPLETE_EVENT_CODED) ? "CODED" : "Unexpected PHY Value");
                        }
                        peripheral_updatePHYStat(HCI_BLE_PHY_UPDATE_COMPLETE_EVENT, (uint8_t *)pMsg);
                    }
                    break;
                }

                default:
                break;
            }

            break;
        }

        default:
        // do nothing
        break;
    }
}

// #ifdef VVTK_NFC
// void Peripheral_setNFCCharValue(void *value , uint8 len)
// {
//     SimpleProfile_SetParameter(NFC_CHAR, len  ,value);
// }
// #endif
#ifdef VVTK_RFID
void Peripheral_setRFIDCharValue(void *value , uint8 len)
{
    SimpleProfile_SetParameter(RFID_CHAR, len  ,value);
}
#endif
// #ifdef VVTK_BLE_OBSERVER
// void Peripheral_setBLECharValue(void *value , uint8 len)
// {
//     SimpleProfile_SetParameter(BLE_CHAR, len  ,value);
// }
// #endif