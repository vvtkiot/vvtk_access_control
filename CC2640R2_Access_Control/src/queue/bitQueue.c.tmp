#include <icall.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>

#include "src/queue/bitQueue.h"
#include "src/system/log.h"

Bit_Queue_Handle Bit_Queue_Init(uint32_t size)
{
    Bit_Queue_Handle handle = ICall_malloc(sizeof(Bit_Queue_Cinfig));
    if(size % 8 == 0)
    {
        handle->buf = ICall_malloc(size/8);
    }
    else
    {
        handle->buf = ICall_malloc(size/8+1);
    }
    handle->size = size ;
    handle->rear = 0 ;
    handle->front = 0;

    Semaphore_Params semParams;
    Semaphore_Params_init(&semParams);
    Semaphore_construct(&handle->semStruct, 1, &semParams);
    handle->semHandle = Semaphore_handle(&handle->semStruct);

    return handle ;
}

bool PushBit(Bit_Queue_Handle handle , BitType data)
{
    Semaphore_pend(handle->semHandle, BIOS_WAIT_FOREVER);
    uint32_t next = (handle->rear+1)%handle->size ;
    if(next == handle->front)
    {
        VVTK_LOGW("bit queue full");
        Semaphore_post(handle->semHandle);
        return false;
    }
    uint32_t index = next / 8 ;
    uint32_t n = 7-(next % 8);
    if(data == ONE)
    {
        handle->buf[index] = handle->buf[index] | ( 0x01<< n);
    }
    else if(data == ZERO)
    {
        handle->buf[index] = handle->buf[index] & (~(0x01 << n));
    }

    handle->rear = next;
    Semaphore_post(handle->semHandle);
    return true;
}

bool PopBit(Bit_Queue_Handle handle , BitType* data)
{
    Semaphore_pend(handle->semHandle, BIOS_WAIT_FOREVER);
    if(handle->rear == handle->front)
    {
        VVTK_LOGW( "bit queue empty");
        Semaphore_post(handle->semHandle);
        return false;
    }

    uint32_t index = handle->front / 8 ;
    uint32_t n = 7-( handle->front % 8);
    uint32_t value = (handle->buf[index]>>n) & 0x01 ;
    if(value == 1)
    {
        *data = ONE ;
    }
    else
    {
        *data = ZERO ;
    }
    handle->front = (handle->front + 1)%handle->size;
    Semaphore_post(handle->semHandle);
    return true;
}