#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>

void Sleep(unsigned int ms)
{
    Task_sleep(ms*1000 / Clock_tickPeriod);
}