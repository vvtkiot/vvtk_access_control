#include <stdbool.h>
#include <ti/drivers/I2C.h>

static I2C_Handle i2c ;
static I2C_Params i2cParams;
static uint8_t slaveAddress ;

bool I2CTool_init(uint8_t index , I2C_BitRate bitRate , uint8_t slave_Address)
{
    I2C_init();
    i2c = NULL;
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = bitRate;
    slaveAddress = slave_Address;
    i2c = I2C_open(index, &i2cParams);
    if(i2c == NULL)
    {
        return false;
    }
    return true;
}

bool I2CTool_send(uint8_t* txBuffer , uint8_t writeCount)
{
    I2C_Transaction i2cTransaction;
    i2cTransaction.slaveAddress = slaveAddress;
    i2cTransaction.writeBuf   = txBuffer;
    i2cTransaction.writeCount = writeCount;
    i2cTransaction.readBuf    = NULL;
    i2cTransaction.readCount  = 0;

    if(!I2C_transfer(i2c, &i2cTransaction))
    {
        return false;
    }
    return true;
}

bool I2CTool_read(uint8_t* rxBuffer , size_t readCount)
{
    I2C_Transaction i2cTransaction;
    i2cTransaction.slaveAddress = slaveAddress;
    i2cTransaction.writeBuf   = NULL;
    i2cTransaction.writeCount = 0;
    i2cTransaction.readBuf    = rxBuffer;
    i2cTransaction.readCount  = readCount;

    if(!I2C_transfer(i2c, &i2cTransaction))
    {
        return false;
    }
    return true;
}

bool I2CTool_close()
{
    if(i2c)
    {
        I2C_close(i2c);
        i2c = NULL;
    }
    return true;
}