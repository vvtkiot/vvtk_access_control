#include <stdint.h>

#include <xdc/runtime/Error.h>

#include <ti/sysbios/knl/Clock.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <Board.h>

#include <icall.h>
#include <hal_assert.h>
#include <bcomdef.h>

#ifndef USE_DEFAULT_USER_CFG
#include <ble_user_config.h>
// BLE user defined configuration
icall_userCfg_t user0Cfg = BLE_USER_CFG;
#endif // USE_DEFAULT_USER_CFG

#ifdef VVTK_NFC
#include "src/nfc/nfc_task.h"
#endif
#ifdef VVTK_RFID
#include "src/rfid/em4095.h"
#endif
#include "src/system/log.h"
#include "src/ble/ble.h"
#include "src/tool/tool.h"

extern void AssertHandler(uint8_t assertCause,uint8_t assertSubcause);

static void *access_control_task(void *arg0)
{
    Log_init();
    GPIO_init();
    SPI_init();

    VVTK_LOGI("\n\nSTART");
    #ifdef VVTK_RFID
    uint8_t *rfidResult = malloc(8);
    uint8_t rfidLen = 8;
    SetupRFID();
    #endif
    #ifdef VVTK_NFC
    SetupNFC();
    #endif
    CreateBleTask();

    while(1)
    {
        #ifdef VVTK_RFID
        memset(rfidResult , 0 , rfidLen);
        if(LoopRFID())
        {
            GetRFIDResult( rfidResult , &rfidLen);
            SetRFIDCharValue(rfidResult , rfidLen);
        }
        #endif
        #ifdef VVTK_NFC
        LoopNFC();
        #endif
    }

    #ifdef VVTK_RFID
    free(rfidResult);
    #endif

    return (NULL);
}

int main()
{
  /* Register Application callback to trap asserts raised in the Stack */
  RegisterAssertCback(AssertHandler);

  Board_initGeneral();

#if !defined( POWER_SAVING )
  /* Set constraints for Standby, powerdown and idle mode */
  // PowerCC26XX_SB_DISALLOW may be redundant
  Power_setConstraint(PowerCC26XX_SB_DISALLOW);
  Power_setConstraint(PowerCC26XX_IDLE_PD_DISALLOW);
#endif // POWER_SAVING

    /* Update User Configuration of the stack */
    user0Cfg.appServiceInfo->timerTickPeriod = Clock_tickPeriod;
    user0Cfg.appServiceInfo->timerMaxMillisecond = ICall_getMaxMSecs();

    /* Initialize ICall module */
    ICall_init();

    /* Start tasks of external images - Priority 5 */
    ICall_createRemoteTasks();

    Task_Params taskNfcParams;
    Task_Params_init(&taskNfcParams);
    taskNfcParams.stackSize = 1024;
    taskNfcParams.priority = 1;
    Task_Handle nfcTaskHandle = Task_create((Task_FuncPtr)access_control_task, &taskNfcParams, Error_IGNORE);
    if (nfcTaskHandle == NULL) {
        while (1);
    }

    /* enable interrupts and start SYS/BIOS */
    BIOS_start();

    return(0);
}

void AssertHandler(uint8_t assertCause, uint8_t assertSubcause)
{
    // VVTK_LOGE(">>>STACK ASSERT Cause 0x%02x subCause 0x%02x",
    //            assertCause, assertSubcause);

    // // check the assert cause
    // switch(assertCause)
    // {
    // case HAL_ASSERT_CAUSE_OUT_OF_MEMORY:
    //     VVTK_LOGE("***ERROR***");
    //     VVTK_LOGE(">> OUT OF MEMORY!");
    //     break;

    // case HAL_ASSERT_CAUSE_INTERNAL_ERROR:
    //     // check the subcause
    //     if(assertSubcause == HAL_ASSERT_SUBCAUSE_FW_INERNAL_ERROR)
    //     {
    //         VVTK_LOGE("***ERROR***");
    //         VVTK_LOGE(">> INTERNAL FW ERROR!");
    //     }
    //     else
    //     {
    //         VVTK_LOGE("***ERROR***");
    //         VVTK_LOGE(">> INTERNAL ERROR!");
    //     }
    //     break;

    // case HAL_ASSERT_CAUSE_ICALL_ABORT:
    //     VVTK_LOGE("***ERROR***");
    //     VVTK_LOGE(">> ICALL ABORT!");
    //     //HAL_ASSERT_SPINLOCK;
    //     break;

    // case HAL_ASSERT_CAUSE_ICALL_TIMEOUT:
    //     VVTK_LOGE("***ERROR***");
    //     VVTK_LOGE(">> ICALL TIMEOUT!");
    //     //HAL_ASSERT_SPINLOCK;
    //     break;

    // case HAL_ASSERT_CAUSE_WRONG_API_CALL:
    //     VVTK_LOGE("***ERROR***");
    //     VVTK_LOGE(">> WRONG API CALL!");
    //     //HAL_ASSERT_SPINLOCK;
    //     break;

    // default:
    //     VVTK_LOGE("***ERROR***");
    //     VVTK_LOGE(">> DEFAULT SPINLOCK!");
    //     //HAL_ASSERT_SPINLOCK;
    // }

    return;
}
